
##############################
# Country definition for U01 #
##############################

province = { id = 900 anti_air = 2 ic = 6 landfort = 10 } # ポートサイド
province = { id = 901 anti_air = 2 oilpool = 1000 supplypool = 2000 ic = 4 landfort = 10 } # スエズ
province = { id = 902 ic = 3 } # ディムヤート
province =
{ id       = 903
  anti_air = 2
  ic       = 3
  air_base = { size = 4 current_size = 4 }
}            # カイロ
province =
{ id         = 907
  anti_air   = 5
  ic         = 6
  air_base   = { size = 4 current_size = 4 }
  naval_base = { size = 10 current_size = 10 }
}              # アレクサンドリア
province =
{ id       = 914
  air_base = { size = 2 current_size = 2 }
  ic       = 2
}            # マルサマトルーフ
province =
{ id            = 920
  anti_air      = 2
  ic            = 7
  naval_base    = { size = 4 current_size = 4 }
  air_base      = { size = 4 current_size = 4 }
  landfort      = 3
}                 # トブルク
province =
{ id            = 924
  anti_air      = 1
  ic            = 6
  naval_base    = { size = 1 current_size = 1 }
  air_base      = { size = 4 current_size = 4 }
}                 # ベンガジ
province = { id = 929 ic = 1 } # サート
province =
{ id            = 932
  anti_air      = 1
  ic            = 6
  naval_base    = { size = 2 current_size = 2 }
  air_base      = { size = 10 current_size = 10 }
  oilpool       = 100
  supplypool    = 500
}                 # トリポリ(リビア)
province =
{ id            = 939
  anti_air      = 4
  ic            = 5
  air_base      = { size = 4 current_size = 4 }
  naval_base    = { size = 4 current_size = 4 }
}                 # チュニス
province =
{ id         = 940
  anti_air   = 1
  naval_base = { size = 2 current_size = 2 }
}              # ビゼルト
province = { id = 945 ic = 2 } # アンナバ
province =
{ id            = 949
  anti_air      = 2
  ic            = 5
  air_base      = { size = 2 current_size = 2 }
}                 # アルジェ
province =
{ id         = 950
  anti_air   = 4
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 4 current_size = 4 }
  supplypool = 2000
  oilpool    = 500
}              # オラン
province =
{ id         = 960
  anti_air   = 3
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 4 current_size = 4 }
}              # カサブランカ
province = { id = 966 ic = 2 } # マラケシュ
province = { id = 971 ic = 1 } # エルアイウン
province =
{ id            = 981
  anti_air      = 3
  coastalfort   = 1
  air_base      = { size = 2 current_size = 2 }
  naval_base    = { size = 4 current_size = 4 }
  ic            = 5
}                 # ダカール
province = { id = 987 ic = 2 } # コナクリ
province =
{ id         = 988
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 2 current_size = 2 }
  oilpool    = 100
  supplypool = 500
}              # フリータウン
province =
{ id         = 993
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 1 current_size = 1 }
}              # アビジャン
province =
{ id         = 1002
  anti_air   = 2
  naval_base = { size = 1 current_size = 1 }
  air_base   = { size = 2 current_size = 2 }
  ic         = 2
}              # アクラ
province = { id = 1006 ic = 3 } # コトヌー
province =
{ id         = 1008
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 2 current_size = 2 }
  oilpool    = 100
  supplypool = 500
}              # ラゴス
province = { id = 1030 ic = 2 } # ポートスーダン
province =
{ id         = 1037
  anti_air   = 2
  naval_base = { size = 2 current_size = 2 }
  supplypool = 500
  oilpool    = 100
  ic         = 2
}              # ジブチ
province = { id = 1038 ic = 2 } # アッサブ
province =
{ id         = 1061
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 1 current_size = 1 }
}              # モンバサ
province =
{ id       = 1066
  anti_air = 1
  air_base = { size = 2 current_size = 2 }
}            # ハルツーム
province =
{ id         = 1089
  anti_air   = 1
  naval_base = { size = 1 current_size = 1 }
}              # ヤウンデ
province =
{ id            = 1093
  anti_air      = 1
  naval_base    = { size = 1 current_size = 1 }
}                 # リーブルヴィル
province = { id = 1095 ic = 4 } # マタディ
province = { id = 1098 ic = 2 } # ルルアブール
province = { id = 1100 ic = 3 } # フアンボ
province = { id = 1134 ic = 2 } # アンタナナリボ
province =
{ id          = 1137
  anti_air    = 2
  coastalfort = 1
  air_base    = { size = 2 current_size = 2 }
  naval_base  = { size = 4 current_size = 4 }
  supplypool  = 500
  oilpool     = 100
  ic          = 3
}               # タマタブ(トアマシナ/マダガスカル)
province = { id = 1800 ic = 3 } # エルアリシュ
province = { id = 1801 ic = 1 } # シャルムエルシェイク
province = { id = 2332 ic = 3 } # ナドル

#####################
# Country main data #
#####################

country =
{ tag                 = U01
  manpower            = 650
  energy              = 18000
  metal               = 20000
  rare_materials      = 15000
  oil                 = 18000
  supplies            = 80000
  money               = 1100
  capital             = 907
  regular_id          = U06
  transports          = 800
  escorts             = 600
  nationalprovinces   = { 1000 1001 1002 1003 1004 1005 1006 1007 1008 1009 1010 1011 1012 1013 1017 1020 1021 1023 1024 1025 1026 1027 1028 1029 1030
                          1031 1032 1033 1034 1035 1037 1038 1039 1040 1041 1042 1043 1044 1045 1046 1047 1048 1049 1050 1051 1052 1053 1054 1055 1056
                          1058 1059 1060 1061 1062 1063 1064 1065 1066 1067 1068 1069 1070 1071 1072 1073 1074 1075 1076 1077 1078 1079 1080 1081 1083
                          1084 1085 1086 1087 1088 1089 1090 1091 1092 1093 1094 1095 1096 1097 1098 1099 1100 1101 1102 1103 1104 1105 1111 1112 1121
                          1123 1124 1125 1126 1127 1128 1129 1130 1131 1132 1133 1134 1137 1138 1139 1140 1141 1142 1143 1144 1145 1146 1816 2021 2024
                          2283 2284 2331 2332 2334 2339 2342 2538 4    900  901  902  903  904  905  906  907  909  910  911  912  913  914  915  916 
                          917  919  920  921  922  923  924  925  926  927  928  929  930  931  932  933  934  935  936  937  938  939  1115 1116 1114
                          1117 1118 1120 1113 1119 1109 1108 1107 1106 1110 1122 1820 975  940  941  942  943  944  945  946  947  948  949  950  951 
                          952  953  955  956  958  959  960  966  967  968  969  971  972  973  974  976  977  978  979  980  981  982  983  984  985 
                          986  987  988  990  991  992  993  994  995  996  997  998  999 
                        }
  ownedprovinces      = { 1000 1001 1002 1003 1004 1005 1006 1007 1008 1009 1010 1011 1012 1013 1017 1020 1021 1023 1024 1025 1026 1027 1028 1029 1030
                          1031 1032 1033 1034 1035 1037 1038 1039 1040 1041 1042 1043 1044 1045 1046 1047 1048 1049 1050 1051 1052 1053 1054 1055 1056
                          1058 1059 1060 1061 1062 1063 1064 1065 1066 1067 1068 1069 1070 1071 1072 1073 1074 1075 1076 1077 1078 1079 1080 1081 1083
                          1084 1085 1086 1087 1088 1089 1090 1091 1092 1093 1094 1095 1096 1097 1098 1099 1100 1101 1102 1103 1104 1105 1111 1112 1121
                          1123 1124 1125 1126 1127 1128 1129 1130 1131 1132 1133 1134 1137 1138 1139 1140 1141 1142 1143 1144 1145 1146 1816 2021 2024
                          2283 2284 2331 2332 2334 2339 2342 2538 4    900  901  902  903  904  905  906  907  909  910  911  912  913  914  915  916 
                          917  919  920  921  922  923  924  925  926  927  928  929  930  931  932  933  934  935  936  937  938  939  1115 1116 1114
                          1117 1118 1120 1113 1119 1109 1108 1107 1106 1110 1122 1820 975  940  941  942  943  944  945  946  947  948  949  950  951 
                          952  953  955  956  958  959  960  966  967  968  969  971  972  973  974  976  977  978  979  980  981  982  983  984  985 
                          986  987  988  990  991  992  993  994  995  996  997  998  999 
                        }
  controlledprovinces = { 1000 1001 1002 1003 1004 1005 1006 1007 1008 1009 1010 1011 1012 1013 1017 1020 1021 1023 1024 1025 1026 1027 1028 1029 1030
                          1031 1032 1033 1034 1035 1037 1038 1039 1040 1041 1042 1043 1044 1045 1046 1047 1048 1049 1050 1051 1052 1053 1054 1055 1056
                          1058 1059 1060 1061 1062 1063 1064 1065 1066 1067 1068 1069 1070 1071 1072 1073 1074 1075 1076 1077 1078 1079 1080 1081 1083
                          1084 1085 1086 1087 1088 1089 1090 1091 1092 1093 1094 1095 1096 1097 1098 1099 1100 1101 1102 1103 1104 1105 1111 1112 1121
                          1123 1124 1125 1126 1127 1128 1129 1130 1131 1132 1133 1134 1137 1138 1139 1140 1141 1142 1143 1144 1145 1146 1816 2021 2024
                          2283 2284 2331 2332 2334 2339 2342 2538 4    900  901  902  903  904  905  906  907  909  910  911  912  913  914  915  916 
                          917  919  920  921  922  923  924  925  926  927  928  929  930  931  932  933  934  935  936  937  938  939  1115 1116 1114
                          1117 1118 1120 1113 1119 1109 1108 1107 1106 1110 1122 1820 975  940  941  942  943  944  945  946  947  948  949  950  951 
                          952  953  955  956  958  959  960  966  967  968  969  971  972  973  974  976  977  978  979  980  981  982  983  984  985 
                          986  987  988  990  991  992  993  994  995  996  997  998  999 
                        }
  techapps            = { 10050 10060 10070 1010  10150 10190 1020  10200 10210 1030  1040  10490 1110  1120  1130  1210  1220  1230  1530  1550 
                          1560  1570  1580  1650  1660  1670  1750  1760  1850  1860  1950  2010  2020  20210 2030  20310 20410 2610  2910  3010 
                          3020  3030  3110  3120  3130  3210  3220  3460  3470  3480  3810  3820  3830  3910  3920  3940  40100 4390  4400  4410 
                          4420  4480  4490  4560  4570  50020 50030 50040 50070 50080 50090 5010  50100 50110 50120 5020  50290 5030  50300 50310
                          5090  5100  5110  5170  5180  5190  5250  5260  5330  5340  5410  5420  5430  5460  5490  5600  5610  5620  5910  5920 
                          5930  7010  80080 80090 8010  8020  8030  8040  8050  8060  8070  8880 
                          8890  8900  8910  8920  9010  9020  9030  9040  9050  9060  9190  9200  9210  9220  9340  9350  9360  9370  9380  9390 
                          9520  9670  9680  9690  9700  9710  9840  9850  9860  10390 4010 4020   4030  4040  4050  4110 4120
                          4130  4140  4150  4210  4220  4230  4240  4250  2040  2050  2060
                          4640  4650  4660  4670  4680  4820  4830  4840  4850  4860  40000 40010 40020 40030 40040
                          6050  6060  6070  6080  6090  6240  6250  6490  6500  6510
                        }
  policy              = { democratic = 3 political_left = 3 freedom = 2 free_market = 8 professional_army = 9 defense_lobby = 8 interventionism = 9 }
free                = { energy = 50 metal = 30 oil = 10 }
inventions             = { 8006 }
  landunit =
  { id       = { type = 24501 id = 5123 }
    name     = "1th Corps"
    location = 958
    home     = 958
    mission  = { type = none target = 958 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5125 }
      name          = "1Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5126 }
    name     = "2th Corps"
    location = 981
    home     = 981
    mission  = { type = none target = 981 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5128 }
      name          = "2Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5129 }
    name     = "3th Corps"
    location = 992
    home     = 992
    mission  = { type = none target = 992 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5131 }
      name          = "3Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5132 }
    name     = "4th Corps"
    location = 1093
    home     = 1093
    mission  = { type = none target = 1093 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5134 }
      name          = "4Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5135 }
    name     = "5th Corps"
    location = 1095
    home     = 1095
    mission  = { type = none target = 1095 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5137 }
      name          = "5Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5138 }
    name     = "6th Corps"
    location = 1096
    home     = 1096
    mission  = { type = none target = 1096 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5140 }
      name          = "6Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5141 }
    name     = "7th Corps"
    location = 1037
    home     = 1037
    mission  = { type = none target = 1037 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5143 }
      name          = "7Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5144 }
    name     = "8th Corps"
    location = 1046
    home     = 1046
    mission  = { type = none target = 1046 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5146 }
      name          = "8Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5147 }
    name     = "9th Corps"
    location = 939
    home     = 939
    mission  = { type = none target = 939 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5149 }
      name          = "9Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5150 }
    name     = "10th Corps"
    location = 949
    home     = 949
    mission  = { type = none target = 949 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5152 }
      name          = "10Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5153 }
    name     = "11th Corps"
    location = 932
    home     = 932
    mission  = { type = none target = 932 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5155 }
      name          = "11Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5156 }
    name     = "12th Corps"
    location = 924
    home     = 924
    mission  = { type = none target = 924 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5158 }
      name          = "12Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5159 }
    name     = "13th Corps"
    location = 920
    home     = 920
    mission  = { type = none target = 920 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5161 }
      name          = "13Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5162 }
    name     = "14th Corps"
    location = 907
    home     = 907
    mission  = { type = none target = 907 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5164 }
      name          = "14Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5165 }
    name     = "15th Corps"
    location = 903
    home     = 903
    mission  = { type = none target = 903 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5167 }
      name          = "15Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5168 }
    name     = "16th Corps"
    location = 1134
    home     = 1134
    mission  = { type = none target = 1134 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5170 }
      name          = "16Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5171 }
    name     = "Middle East HQ"
    location = 900
    home     = 900
    mission  = { type = none target = 1803 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5173 }
      name          = "1th HQ"
      model         = 2
      brigade_model = 0
      type          = hq
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5189 }
    name     = "20th Corps"
    location = 900
    home     = 900
    division =
    { id            = { type = 24501 id = 5191 }
      name          = "1th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5192 }
      name          = "2th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5193 }
      name          = "2Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5194 }
    name     = "21th Corps"
    location = 901
    home     = 901
    division =
    { id            = { type = 24501 id = 5196 }
      name          = "3th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5197 }
      name          = "4th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5198 }
      name          = "3Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5199 }
    name     = "Suez Defense HQ"
    location = 903
    home     = 903
    mission  = { type = none target = 903 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5201 }
      name          = "2th HQ"
      model         = 2
      brigade_model = 0
      type          = hq
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5202 }
    name     = "22th Corps"
    location = 901
    home     = 901
    mission  = { type = none target = 901 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5204 }
      name          = "5th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5205 }
      name          = "6th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5206 }
      name          = "4Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 15207 }
    name     = "23th Corps"
    location = 901
    home     = 901
    division =
    { id            = { type = 24501 id = 15209 }
      name          = "7th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 35209 }
      name          = "8th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15210 }
      name          = "5Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 15211 }
    name     = "24th Corps"
    location = 901
    home     = 901
    division =
    { id            = { type = 24501 id = 15212 }
      name          = "9th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15213 }
      name          = "10th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15214 }
      name          = "6Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 15215 }
    name     = "25th Corps"
    location = 901
    home     = 901
    division =
    { id            = { type = 24501 id = 15216 }
      name          = "11th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15217 }
      name          = "12th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15218 }
      name          = "7Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 15219 }
    name     = "26th Corps"
    location = 900
    home     = 900
    division =
    { id            = { type = 24501 id = 15220 }
      name          = "13th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15221 }
      name          = "14th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15222 }
      name          = "8Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 15223 }
    name     = "27th Corps"
    location = 900
    home     = 900
    division =
    { id            = { type = 24501 id = 15224 }
      name          = "15th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15225 }
      name          = "16th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15226 }
      name          = "9Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 15227 }
    name     = "28th Corps"
    location = 900
    home     = 900
    division =
    { id            = { type = 24501 id = 15228 }
      name          = "17th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15229 }
      name          = "18th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 15230 }
      name          = "10Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5207 }
    name     = "23th Corps"
    location = 900
    home     = 900
    mission  = { type = none target = 900 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5209 }
      name          = "7th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5210 }
      name          = "8th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5211 }
      name          = "5Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 25212 }
    name     = "24th Corps"
    location = 901
    home     = 901
    mission  = { type = none target = 901 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 25214 }
      name          = "6Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 25215 }
      name          = "7Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 25216 }
      name          = "8Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 25217 }
    name     = "25th Corps"
    location = 900
    home     = 900
    division =
    { id            = { type = 24501 id = 25219 }
      name          = "9Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 25220 }
      name          = "3Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 25221 }
      name          = "4Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 25222 }
    name     = "26th Corps"
    location = 900
    home     = 900
    mission  = { type = none target = 900 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 25224 }
      name          = "7th Mechanized Armored Infantry"
      model         = 2
      brigade_model = 0
      type          = mechanized
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 25225 }
      name          = "8th Mechanized Armored Infantry"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 25226 }
      name          = "9th Mechanized Armored Infantry"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 25227 }
    name     = "27th Corps"
    location = 901
    home     = 901
    mission  = { type = none target = 901 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 25229 }
      name          = "10th Mechanized Armored Infantry"
      model         = 2
      brigade_model = 0
      type          = mechanized
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 25230 }
      name          = "11th Mechanized Armored Infantry"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 25231 }
      name          = "12th Mechanized Armored Infantry"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6182 }
    name     = "AfricaOcean Army  3th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5251 }
      name     = "1th Missile Boat Division"
      model    = 3
      type     = light_cruiser
      strength = 100.0000
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6184 }
    name     = "AfricaOcean Army  4th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5261 }
      name     = "2th Destroyer Flotilla"
      model    = 3
      type     = destroyer
      strength = 100.0000
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6186 }
    name     = "AfricaOcean Army  5th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5262 }
      name     = "3th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength = 100.0000
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6188 }
    name     = "AfricaOcean Army  6th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5263 }
      name     = "4th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength = 100.0000
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6190 }
    name     = "AfricaOcean Army  7th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5264 }
      name     = "5th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength = 100.0000
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6192 }
    name     = "AfricaOcean Army  8th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5266 }
      name     = "7th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength = 100.0000
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6194 }
    name     = "AfricaOcean Army  9th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5270 }
      name     = "1th Transport Ship Division"
      model    = 4
      type     = transport
      strength = 100.0000
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6196 }
    name     = "AfricaOcean Army  10th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5271 }
      name     = "2th Transport Ship Division"
      model    = 4
      type     = transport
      strength = 100.0000
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6198 }
    name     = "Africa Ocean Army 11th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5272 }
      name     = "3th Transport Ship Division"
      model    = 4
      type     = transport
      strength = 100.0000
    }
  }
  navalunit =
  { id       = { type = 24502 id = 2527 }
    name     = "AfricaOcean Army  1th Fleet"
    location = 907
    home     = 907
    base     = 907
    division =
    { id       = { type = 24502 id = 5247 }
      name     = "1MeguyoWarshipDivision"
      model    = 3
      type     = heavy_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5248 }
      name     = "2MeguyoWarshipDivision"
      model    = 3
      type     = heavy_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5249 }
      name     = "3MeguyoWarshipDivision"
      model    = 2
      type     = heavy_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5250 }
      name     = "4MeguyoWarshipDivision"
      model    = 2
      type     = heavy_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5252 }
      name     = "2th Missile Boat Division"
      model    = 3
      type     = light_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5253 }
      name     = "3th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5254 }
      name     = "4th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5255 }
      name     = "5th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5256 }
      name     = "6th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5257 }
      name     = "7th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5258 }
      name     = "8th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5259 }
      name     = "9th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5260 }
      name     = "1th Destroyer Flotilla"
      model    = 3
      type     = destroyer
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5265 }
      name     = "6th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5267 }
      name     = "8th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5268 }
      name     = "9th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5269 }
      name     = "10th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5273 }
      name     = "1Submarine Warshipth Fleet"
      model    = 2
      type     = submarine
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5274 }
      name     = "2Submarine Warshipth Fleet"
      model    = 2
      type     = submarine
      strength = 100.0000
    }
    division =
    { id       = { type = 24502 id = 5275 }
      name     = "3Submarine Warshipth Fleet"
      model    = 2
      type     = submarine
      strength = 100.0000
    }
  }
}
