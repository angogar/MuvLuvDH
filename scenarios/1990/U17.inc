
##############################
# Country definition for U17 #
##############################

province =
{ id              = 1551
  air_base        = 10
  radar_station   = 10
  
  rocket_test     = 10
  infra           = 2.0
  anti_air        = 10
}                   # エキバストス(カザフ)

#####################
# Country main data #
#####################

country =
{ tag                 = U17
  manpower            = 99999
  energy              = 900000
  metal               = 900000
  rare_materials      = 900000
  oil                 = 900000
  supplies            = 900000
  money               = 900000
  capital             = 1551
  transports          = 9999
  escorts             = 9999
  extra_tc            = 9999
  policy =
  { democratic        = 1
    political_left    = 1
    freedom           = 1
    free_market       = 1
    professional_army = 1
    defense_lobby     = 10
    interventionism   = 10
  }
  nationalprovinces   = { 1551 }
  ownedprovinces      = { 1551 }
  controlledprovinces = { 1551 1422 1423 1424 1425 1426 1550 1555 1556 1569 1576 1578 1579 1580 1581 1582 1583 1586 1587 1588 }
  techapps            = { 7020 }
  free                = { energy = 1000 manpower = 9 metal = 1000 rare_materials = 1000 oil = 1000 money = 1000 ic = 300 supplies = 1000 }
  landunit =
  { id       = { type = 15251 id = 4078 }
    name     = "3th Corps"
    location = 1539
    division =
    { id            = { type = 15251 id = 4080 }
      name          = "3th Commander Group"
      model         = 33
      brigade_model = 0
      extra         = light_armor_brigade
      type          = hq
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15251 id = 4081 }
    name     = "4th Corps"
    location = 1545
    division =
    { id            = { type = 15251 id = 4083 }
      name          = "1th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4086 }
      name          = "1th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4089 }
      name          = "4th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4090 }
      name          = "5th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4091 }
      name          = "6th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15251 id = 4097 }
    name     = "5th Corps"
    location = 1546
    division =
    { id            = { type = 15251 id = 4099 }
      name          = "12th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4102 }
      name          = "4th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4105 }
      name          = "15th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4106 }
      name          = "16th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4107 }
      name          = "17th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15251 id = 4113 }
    name     = "6th Corps"
    location = 1559
    division =
    { id            = { type = 15251 id = 4115 }
      name          = "23th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4118 }
      name          = "7th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4121 }
      name          = "26th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4122 }
      name          = "27th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15251 id = 4123 }
      name          = "28th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15251 id = 1 }
    name     = "Hive"
    location = 1551
    division =
    { type   = garrison
      model  = 14
      id     = { type = 15254 id = 1 }
      name   = "Hive"
      locked = yes
    }
    division =
    { type   = garrison
      model  = 33
      id     = { type = 15254 id = 2 }
      name   = "Gate-Class"
      locked = yes
    }
  }
}
