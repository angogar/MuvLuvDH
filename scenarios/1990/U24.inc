
##############################
# Country definition for U24 #
##############################

province = { id = 722 air_base = 10 radar_station = 10  rocket_test = 10 infra = 2.0  anti_air = 10 } # ボーパール

#####################
# Country main data #
#####################

country =
{ tag                 = U24
  manpower            = 99999
  energy              = 900000
  metal               = 900000
  rare_materials      = 900000
  oil                 = 900000
  supplies            = 900000
  money               = 900000
  capital             = 722
  transports          = 9999
  escorts             = 9999
  extra_tc            = 9999
  policy =
  { democratic        = 1
    political_left    = 1
    freedom           = 1
    free_market       = 1
    professional_army = 1
    defense_lobby     = 10
    interventionism   = 10
  }
  nationalprovinces   = { 722 }
  ownedprovinces      = { 722 }
  controlledprovinces = { 722 1421 1432 1433 1435 1445 1446 1447 1448 1450 1451 1452 1728 1731 1430 1474 736 1705 593 1465 1466 1479 1721 
                          1478 1468 1592 685 687 1476 1477 1469 1470 2092 1472 1473 2199 1455 2043 1456 1464 631 611 1467 1460 714 599 2133 1471 }
  techapps            = { 7020 }
  free                = { energy = 1000 manpower = 9 metal = 1000 rare_materials = 1000 oil = 1000 money = 1000 ic = 300 supplies = 1000 }
  landunit =
  { id       = { type = 15551 id = 4072 }
    name     = "1th Corps"
    location = 1721
    division =
    { id            = { type = 15551 id = 4074 }
      name          = "1th Commander Group"
      model         = 33
      type          = hq
      strength      = 100
      brigade_model = 0
      extra         = light_armor_brigade
    }
  }
  landunit =
  { id       = { type = 15551 id = 4075 }
    name     = "2th Corps"
    location = 599
    division =
    { id            = { type = 15551 id = 4077 }
      name          = "2th Commander Group"
      model         = 33
      brigade_model = 0
      extra         = light_armor_brigade
      type          = hq
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 4078 }
    name     = "3th Corps"
    location = 2043
    division =
    { id            = { type = 15551 id = 4080 }
      name          = "3th Commander Group"
      model         = 33
      brigade_model = 0
      extra         = light_armor_brigade
      type          = hq
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 4081 }
    name     = "4th Corps"
    location = 1464
    division =
    { id            = { type = 15551 id = 4083 }
      name          = "1th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4086 }
      name          = "1th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4089 }
      name          = "4th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4090 }
      name          = "5th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4091 }
      name          = "6th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 4097 }
    name     = "5th Corps"
    location = 631
    division =
    { id            = { type = 15551 id = 4099 }
      name          = "12th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4102 }
      name          = "4th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4105 }
      name          = "15th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4106 }
      name          = "16th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4107 }
      name          = "17th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 4113 }
    name     = "6th Corps"
    location = 714
    division =
    { id            = { type = 15551 id = 4115 }
      name          = "23th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4118 }
      name          = "7th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4121 }
      name          = "26th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4122 }
      name          = "27th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4123 }
      name          = "28th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 4129 }
    name     = "7th Corps"
    location = 1460
    division =
    { id            = { type = 15551 id = 4131 }
      name          = "34th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4134 }
      name          = "10th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4137 }
      name          = "37th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4138 }
      name          = "38th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4139 }
      name          = "39th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 4145 }
    name     = "8th Corps"
    location = 2133
    division =
    { id            = { type = 15551 id = 4147 }
      name          = "45th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4150 }
      name          = "13th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4153 }
      name          = "48th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4154 }
      name          = "49th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4155 }
      name          = "50th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 4161 }
    name     = "9th Corps"
    location = 1472
    division =
    { id            = { type = 15551 id = 4163 }
      name          = "56th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4166 }
      name          = "16th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4169 }
      name          = "59th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4170 }
      name          = "60th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4171 }
      name          = "61th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 4177 }
    name     = "10th Corps"
    location = 1456
    division =
    { id            = { type = 15551 id = 4179 }
      name          = "67th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4182 }
      name          = "19th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4185 }
      name          = "70th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4186 }
      name          = "71th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 4187 }
      name          = "72th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5476 }
    name     = "42th Corps"
    location = 1464
    division =
    { id            = { type = 15551 id = 5478 }
      name          = "43th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5479 }
      name          = "43th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5480 }
      name          = "119th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5481 }
      name          = "120th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5482 }
      name          = "121th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5483 }
    name     = "43th Corps"
    location = 631
    division =
    { id            = { type = 15551 id = 5485 }
      name          = "44th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5486 }
      name          = "44th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5487 }
      name          = "122th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5488 }
      name          = "123th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5489 }
      name          = "124th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5490 }
    name     = "44th Corps"
    location = 714
    division =
    { id            = { type = 15551 id = 5492 }
      name          = "45th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5493 }
      name          = "45th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5494 }
      name          = "125th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5495 }
      name          = "126th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5496 }
      name          = "127th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5497 }
    name     = "45th Corps"
    location = 1460
    division =
    { id            = { type = 15551 id = 5499 }
      name          = "46th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5500 }
      name          = "46th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5501 }
      name          = "128th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5502 }
      name          = "129th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5503 }
      name          = "130th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5504 }
    name     = "46th Corps"
    location = 2133
    division =
    { id            = { type = 15551 id = 5506 }
      name          = "47th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5507 }
      name          = "47th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5508 }
      name          = "131th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5509 }
      name          = "132th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5510 }
      name          = "133th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5511 }
    name     = "47th Corps"
    location = 1472
    division =
    { id            = { type = 15551 id = 5513 }
      name          = "48th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5514 }
      name          = "48th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5515 }
      name          = "134th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5516 }
      name          = "135th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5517 }
      name          = "136th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5518 }
    name     = "48th Corps"
    location = 1456
    division =
    { id            = { type = 15551 id = 5520 }
      name          = "49th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5521 }
      name          = "49th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5522 }
      name          = "137th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5523 }
      name          = "138th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5524 }
      name          = "139th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5525 }
    name     = "49th Corps"
    location = 1455
    division =
    { id            = { type = 15551 id = 5527 }
      name          = "50th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5528 }
      name          = "50th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5529 }
      name          = "140th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5530 }
      name          = "141th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15551 id = 5531 }
      name          = "142th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5467 }
    name     = "39th Corps"
    location = 1721
    division =
    { id            = { type = 15551 id = 5469 }
      name          = "13th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5470 }
    name     = "40th Corps"
    location = 599
    division =
    { id            = { type = 15551 id = 5472 }
      name          = "14th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = super_heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 5473 }
    name     = "41th Corps"
    location = 2043
    division =
    { id            = { type = 15551 id = 5475 }
      name          = "15th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15551 id = 1 }
    name     = "HIVE"
    location = 722
    division =
    { type   = garrison
      model  = 13
      id     = { type = 15554 id = 1 }
      name   = "HIVE"
      locked = yes
    }
    division =
    { type   = garrison
      model  = 33
      id     = { type = 15554 id = 2 }
      name   = "Gate-Class"
      locked = yes
    }
  }
}
