
##############################
# Country definition for U20 #
##############################

province = { id = 1864 air_base = 10 radar_station = 10  rocket_test = 10  anti_air = 10 infra = 2.0 } # アンバール

#####################
# Country main data #
#####################

country =
{ tag                 = U20
  manpower            = 99999
  energy              = 900000
  metal               = 900000
  rare_materials      = 900000
  oil                 = 900000
  supplies            = 900000
  money               = 900000
  capital             = 1864
  transports          = 9999
  escorts             = 9999
  extra_tc            = 9999
  policy =
  { democratic        = 1
    political_left    = 1
    freedom           = 1
    free_market       = 1
    professional_army = 1
    defense_lobby     = 10
    interventionism   = 10
  }
  nationalprovinces   = { 1864 }
  ownedprovinces      = { 1864 }
  controlledprovinces = { 1864 1862 1863 1792 1861 407  1159 2019 1793 1865 1866 1790 1824 1806 1791 710  1860 2496 1498
                          409  408  437
                          438  439  441  442  443  448  440  449  447  446  445  450  1851 1852 1853 1854 1855 2322 1158
                          1503 1502 1788 1789 1825 1497 1499 1826 1501 1496 1495 1859 1787 }
  techapps            = { 7020 }
  free                = { energy = 1000 manpower = 9 metal = 1000 rare_materials = 1000 oil = 1000 money = 1000 ic = 300 supplies = 1000 }
  landunit =
  { id       = { type = 15401 id = 4072 }
    name     = "1th Corps"
    location = 442
    division =
    { id            = { type = 15401 id = 4074 }
      name          = "1th Commander Group"
      model         = 33
      type          = hq
      strength      = 100
      brigade_model = 0
      extra         = light_armor_brigade
    }
  }
  landunit =
  { id       = { type = 15401 id = 4075 }
    name     = "2th Corps"
    location = 1848
    division =
    { id            = { type = 15401 id = 4077 }
      name          = "2th Commander Group"
      model         = 33
      brigade_model = 0
      extra         = light_armor_brigade
      type          = hq
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15401 id = 4078 }
    name     = "3th Corps"
    location = 1864
    division =
    { id            = { type = 15401 id = 4080 }
      name          = "3th Commander Group"
      model         = 33
      brigade_model = 0
      extra         = light_armor_brigade
      type          = hq
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15401 id = 4081 }
    name     = "4th Corps"
    location = 408
    division =
    { id            = { type = 15401 id = 4083 }
      name          = "1th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4086 }
      name          = "1th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4089 }
      name          = "4th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4090 }
      name          = "5th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4091 }
      name          = "6th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15401 id = 4097 }
    name     = "5th Corps"
    location = 1855
    division =
    { id            = { type = 15401 id = 4099 }
      name          = "12th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4102 }
      name          = "4th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4105 }
      name          = "15th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4106 }
      name          = "16th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4107 }
      name          = "17th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15401 id = 4113 }
    name     = "6th Corps"
    location = 449
    division =
    { id            = { type = 15401 id = 4115 }
      name          = "23th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4118 }
      name          = "7th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4121 }
      name          = "26th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4122 }
      name          = "27th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4123 }
      name          = "28th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15401 id = 4129 }
    name     = "7th Corps"
    location = 378
    division =
    { id            = { type = 15401 id = 4131 }
      name          = "34th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4134 }
      name          = "10th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4137 }
      name          = "37th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4138 }
      name          = "38th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4139 }
      name          = "39th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15401 id = 4145 }
    name     = "8th Corps"
    location = 378
    division =
    { id            = { type = 15401 id = 4147 }
      name          = "45th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4150 }
      name          = "13th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4153 }
      name          = "48th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4154 }
      name          = "49th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4155 }
      name          = "50th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15401 id = 4161 }
    name     = "9th Corps"
    location = 423
    division =
    { id            = { type = 15401 id = 4163 }
      name          = "56th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4166 }
      name          = "16th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4169 }
      name          = "59th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4170 }
      name          = "60th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15401 id = 4171 }
      name          = "61th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15401 id = 1 }
    name     = "Hive"
    location = 1864
    division =
    { type   = garrison
      model  = 12
      id     = { type = 15404 id = 1 }
      name   = "Hive"
      locked = yes
    }
    division =
    { type   = garrison
      model  = 33
      id     = { type = 15404 id = 2 }
      name   = "Gate-Class"
      locked = yes
    }
  }
}
