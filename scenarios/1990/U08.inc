
##############################
# Country definition for BRA #
##############################

province =
{ id       = 826
  air_base = { size = 1 current_size = 1 }
}            # レシフェ
province = { id = 847 anti_air = 1 ic = 3 } # クリチバ
province =
{ id         = 848
  anti_air   = 2
  air_base   = { size = 4 current_size = 4 }
  naval_base = { size = 4 current_size = 4 }
  ic         = 2
}              # ポルト・アレグレ
province =
{ id         = 877
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 2 current_size = 2 }
}              # ベレン
province =
{ id       = 880
  air_base = { size = 1 current_size = 1 }
  ic       = 2
}            # フォルタレーザ
province =
{ id         = 881
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 4 current_size = 4 }
  ic         = 4
}              # ナタール
province =
{ id         = 882
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 2 current_size = 2 }
  ic         = 2
}              # サルバドール
province = { id = 883 ic = 1 } # ゴイアス
province =
{ id            = 885
  anti_air      = 2
  air_base      = { size = 4 current_size = 4 }
  naval_base    = { size = 6 current_size = 6 }
  ic            = 8
}                 # リオデジャネイロ
province =
{ id       = 886
  anti_air = 1
  air_base = { size = 4 current_size = 4 }
  ic       = 6
}            # サンパウロ
province = { id = 887 anti_air = 1 } # ベロ・オリゾンテ

#####################
# Country main data #
#####################

country =
{ tag                 = U08
  energy              = 5000
  metal               = 5000
  rare_materials      = 5000
  oil                 = 5000
  supplies            = 8000
  manpower            = 156
  capital             = 886
  transports          = 50
  escorts             = 20
  money               = 350
  regular_id = BRA
  nationalprovinces   = { 839 840 842 856 860 861 864 865 848 847 835 834 825 823 822 820 872 824 826 878 876 877 879 883 887 886 885 884 882 880
                          881 667 853 852 843 844 845 851 854 855 857 858 859 862 863 850 803 812 813 819 867 868 890 802 804 810 811 869 870 816
                          821 829 830 831 814 815 817 818 551 833 849 837 846 827 828 832 836 838 875 874 873 871 }
  ownedprovinces      = { 839 840 842 856 860 861 864 865 848 847 835 834 825 823 822 820 872 824 826 878 876 877 879 883 887 886 885 884 882 880
                          881 667 853 852 843 844 845 851 854 855 857 858 859 862 863 850 803 812 813 819 867 868 890 802 804 810 811 869 870 816
                          821 829 830 831 814 815 817 818 551 833 849 837 846 827 828 832 836 838 875 874 873 871 }
  controlledprovinces = { 839 840 842 856 860 861 864 865 848 847 835 834 825 823 822 820 872 824 826 878 876 877 879 883 887 886 885 884 882 880
                          881 667 853 852 843 844 845 851 854 855 857 858 859 862 863 850 803 812 813 819 867 868 890 802 804 810 811 869 870 816
                          821 829 830 831 814 815 817 818 551 833 849 837 846 827 828 832 836 838 875 874 873 871 }
  techapps            = { 10050 1010  10190 1020  1110  1120  1210  1550  1650  3010  3020  3110  3460  3810  3910  4390  50020 5010  5020  50290
                          50300 5090  5410  5600  6050  6060  6070  6080  6090  7010  80080 8010  8020  8030  8040  8050  8060  8070  8080  9010 
                          9340 
                        }
  policy              = { democratic = 3 political_left = 4 freedom = 3 free_market = 7 professional_army = 6 defense_lobby = 7 interventionism = 6 }
  landunit =
  { id       = { type = 23601 id = 6122 }
    name     = "1th Corps"
    location = 877
    home     = 877
    mission  = { type = none target = 877 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 23601 id = 6124 }
      name          = "1th SP Support Division"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
      locked        = yes
    }
  }
  landunit =
  { id       = { type = 23601 id = 6125 }
    name     = "2th Corps"
    location = 826
    home     = 826
    mission  = { type = none target = 826 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 23601 id = 6127 }
      name          = "2th SP Support Division"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
      locked        = yes
    }
  }
  landunit =
  { id       = { type = 23601 id = 6128 }
    name     = "3th Corps"
    location = 882
    home     = 882
    mission  = { type = none target = 882 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 23601 id = 6130 }
      name          = "3th SP Support Division"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
      locked        = yes
    }
  }
  landunit =
  { id       = { type = 23601 id = 6131 }
    name     = "4th Corps"
    location = 884
    home     = 884
    mission  = { type = none target = 884 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 23601 id = 6133 }
      name          = "4th SP Support Division"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
      locked        = yes
    }
  }
  landunit =
  { id       = { type = 23601 id = 6134 }
    name     = "5th Corps"
    location = 886
    home     = 886
    mission  = { type = none target = 886 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 23601 id = 6136 }
      name          = "5th SP Support Division"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
      locked        = yes
    }
  }
  landunit =
  { id       = { type = 23601 id = 6137 }
    name     = "6th Corps"
    location = 883
    home     = 883
    mission  = { type = none target = 883 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 23601 id = 6139 }
      name          = "6th SP Support Division"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
      locked        = yes
    }
  }
  landunit =
  { id       = { type = 23601 id = 6140 }
    name     = "7th Corps"
    location = 885
    home     = 885
    mission  = { type = none target = 885 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 23601 id = 6142 }
      name          = "7th SP Support Division"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
      locked        = yes
    }
  }
  landunit =
  { id       = { type = 23601 id = 6143 }
    name     = "8th Corps"
    location = 885
    home     = 885
    mission  = { type = none target = 885 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 23601 id = 6145 }
      name          = "1th Mechanized Infantry Division"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
      locked        = yes
    }
  }
  landunit =
  { id       = { type = 23601 id = 6146 }
    name     = "9th Corps"
    location = 885
    home     = 885
    mission  = { type = none target = 885 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 23601 id = 6148 }
      name          = "1th Tank Division"
      model         = 3
      brigade_model = 0
      type          = armor
      extra         = sp_artillery
      strength      = 100
      locked        = yes
    }
    division =
    { id            = { type = 23601 id = 6149 }
      name          = "1th Infantry Tank Division"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
      locked        = yes
    }
  }
  navalunit =
  { id       = { type = 23602 id = 2377 }
    name     = "1th Squadron"
    location = 885
    home     = 885
    base     = 885
    division =
    { id       = { type = 23602 id = 6150 }
      name     = "1th Missile Boat Division"
      model    = 3
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 23602 id = 6151 }
      name     = "2th Missile Boat Division"
      model    = 3
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 23602 id = 6152 }
      name     = "1th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength            = 100
    }
    division =
    { id       = { type = 23602 id = 6153 }
      name     = "2th Destroyer Flotilla"
      model    = 2
      type     = destroyer
      strength            = 100
    }
    division =
    { id       = { type = 23602 id = 6154 }
      name     = "1th Transport Ship Division"
      model    = 4
      type     = transport
      strength            = 100
    }
  }
}
