
##############################
# Country definition for U12 #
##############################

province = { id = 1431 air_base = 10 radar_station = 10  rocket_test = 10 infra = 2.0  anti_air = 10 } # �\�Y

#####################
# Country main data #
#####################

country =
{ tag                 = U12
  manpower            = 99999
  energy              = 900000
  metal               = 900000
  rare_materials      = 900000
  oil                 = 900000
  supplies            = 900000
  money               = 900000
  capital             = 1431
  transports          = 9999
  escorts             = 9999
  extra_tc            = 9999
  policy =
  { democratic        = 1
    political_left    = 1
    freedom           = 1
    free_market       = 1
    professional_army = 1
    defense_lobby     = 10
    interventionism   = 10
  }
  nationalprovinces   = { 1431 }
  ownedprovinces      = { 1431 }
  controlledprovinces = { 1431 }
  techapps            = { 7020 }
  free                = { energy = 1000 manpower = 9 metal = 1000 rare_materials = 1000 oil = 1000 money = 1000 ic = 500 supplies = 1000 }
  landunit =
  { id       = { type = 15001 id = 5177 }
    name     = "1th Corps"
    location = 183
    division =
    { id            = { type = 15001 id = 5179 }
      name          = "1th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5180 }
    name     = "2th Corps"
    location = 139
    division =
    { id            = { type = 15001 id = 5182 }
      name          = "2th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = super_heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5183 }
    name     = "3th Corps"
    location = 149
    division =
    { id            = { type = 15001 id = 5185 }
      name          = "3th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5221 }
    name     = "9th Corps"
    location = 1792
    division =
    { id            = { type = 15001 id = 5223 }
      name          = "4th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5224 }
    name     = "10th Corps"
    location = 1806
    division =
    { id            = { type = 15001 id = 5226 }
      name          = "5th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = super_heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5282 }
    name     = "20th Corps"
    location = 1543
    division =
    { id            = { type = 15001 id = 5284 }
      name          = "8th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = super_heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5285 }
    name     = "21th Corps"
    location = 1539
    division =
    { id            = { type = 15001 id = 5287 }
      name          = "9th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5316 }
    name     = "26th Corps"
    location = 1546
    division =
    { id            = { type = 15001 id = 5318 }
      name          = "17th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5319 }
      name          = "17th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5320 }
      name          = "49th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5321 }
      name          = "50th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5322 }
      name          = "51th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5323 }
    name     = "27th Corps"
    location = 1559
    division =
    { id            = { type = 15001 id = 5325 }
      name          = "18th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5326 }
      name          = "18th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5327 }
      name          = "52th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5328 }
      name          = "53th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5329 }
      name          = "54th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5330 }
    name     = "28th Corps"
    location = 1448
    division =
    { id            = { type = 15001 id = 5332 }
      name          = "10th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5333 }
    name     = "29th Corps"
    location = 1446
    division =
    { id            = { type = 15001 id = 5335 }
      name          = "11th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = super_heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5336 }
    name     = "30th Corps"
    location = 1556
    division =
    { id            = { type = 15001 id = 5338 }
      name          = "12th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5339 }
    name     = "31th Corps"
    location = 1455
    division =
    { id            = { type = 15001 id = 5341 }
      name          = "19th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5342 }
      name          = "20th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5343 }
      name          = "21th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5344 }
      name          = "19th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5345 }
      name          = "20th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5346 }
      name          = "21th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5347 }
      name          = "55th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5348 }
      name          = "56th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5349 }
      name          = "57th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5350 }
      name          = "58th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5351 }
      name          = "59th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5352 }
      name          = "60th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5353 }
      name          = "61th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5354 }
      name          = "62th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5355 }
    name     = "32th Corps"
    location = 1451
    division =
    { id            = { type = 15001 id = 5357 }
      name          = "22th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5358 }
      name          = "23th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5359 }
      name          = "24th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5360 }
      name          = "22th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5361 }
      name          = "23th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5362 }
      name          = "24th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5363 }
      name          = "63th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5364 }
      name          = "64th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5365 }
      name          = "65th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5366 }
      name          = "66th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5367 }
      name          = "67th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5368 }
      name          = "68th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5369 }
      name          = "69th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5370 }
      name          = "70th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5371 }
    name     = "33th Corps"
    location = 1452
    division =
    { id            = { type = 15001 id = 5373 }
      name          = "25th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5374 }
      name          = "26th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5375 }
      name          = "27th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5376 }
      name          = "25th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5377 }
      name          = "26th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5378 }
      name          = "27th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5379 }
      name          = "71th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5380 }
      name          = "72th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5381 }
      name          = "73th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5382 }
      name          = "74th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5383 }
      name          = "75th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5384 }
      name          = "76th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5385 }
      name          = "77th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5386 }
      name          = "78th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5387 }
    name     = "34th Corps"
    location = 1450
    division =
    { id            = { type = 15001 id = 5389 }
      name          = "28th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5390 }
      name          = "29th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5391 }
      name          = "30th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5392 }
      name          = "28th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5393 }
      name          = "29th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5394 }
      name          = "30th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5395 }
      name          = "79th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5396 }
      name          = "80th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5397 }
      name          = "81th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5403 }
    name     = "35th Corps"
    location = 1445
    division =
    { id            = { type = 15001 id = 5405 }
      name          = "31th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5406 }
      name          = "32th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5407 }
      name          = "33th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5408 }
      name          = "31th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5409 }
      name          = "32th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5410 }
      name          = "33th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5411 }
      name          = "87th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5412 }
      name          = "88th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5413 }
      name          = "89th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5419 }
    name     = "36th Corps"
    location = 1435
    division =
    { id            = { type = 15001 id = 5421 }
      name          = "34th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5422 }
      name          = "35th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5423 }
      name          = "36th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5424 }
      name          = "34th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5430 }
      name          = "98th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5431 }
      name          = "99th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5432 }
      name          = "100th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5433 }
      name          = "101th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5434 }
      name          = "102th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5435 }
    name     = "37th Corps"
    location = 1433
    division =
    { id            = { type = 15001 id = 5442 }
      name          = "39th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5443 }
      name          = "103th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5444 }
      name          = "104th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5445 }
      name          = "105th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5446 }
      name          = "106th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5447 }
      name          = "107th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5448 }
      name          = "108th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5449 }
      name          = "109th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5450 }
      name          = "110th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 5451 }
    name     = "38th Corps"
    location = 1421
    division =
    { id            = { type = 15001 id = 5453 }
      name          = "40th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5454 }
      name          = "41th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5455 }
      name          = "42th BETA Group"
      model         = 33
      brigade_model = 0
      type          = marine
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5456 }
      name          = "40th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5457 }
      name          = "41th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = light_armor_brigade
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5458 }
      name          = "42th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5459 }
      name          = "111th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5460 }
      name          = "112th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = super_heavy_armor
      strength      = 100
    }
    division =
    { id            = { type = 15001 id = 5461 }
      name          = "113th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 15001 id = 1 }
    name     = "Hive"
    location = 1431
    division =
    { type   = garrison
      model  = 14
      id     = { type = 15004 id = 1 }
      name   = "Hive"
      locked = yes
    }
    division =
    { type   = garrison
      model  = 33
      id     = { type = 15004 id = 2 }
      name   = "Gate-Class"
      locked = yes
    }
  }
}
