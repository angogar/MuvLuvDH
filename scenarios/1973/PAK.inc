
##############################
# Country definition for PAK #
##############################

province =
{ id         = 1285
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 1 current_size = 1 }
}              # クルナ
province =
{ id         = 1481
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 1 current_size = 1 }
}              # カラチ

#####################
# Country main data #
#####################

country =
{ tag                 = PAK
  manpower            = 95
  energy              = 800
  metal               = 800
  rare_materials      = 800
  oil                 = 800
  supplies            = 200
  money               = 100
  capital             = 1285
  regular_id          = U04
  transports          = 55
  escorts             = 12
  policy              = { democratic = 2 political_left = 3 freedom = 3 free_market = 3 professional_army = 7 defense_lobby = 8 interventionism = 6 }
  ownedprovinces      = { 1475 1480 1481 1482 1483 1494 1592 1731 1474 }
  controlledprovinces = { 1475 1480 1481 1482 1483 1494 1592 1731 1474 }
  techapps            = { 10050 1010  10190 1020  1110  1120  1210  1550  1650  3010  3020  3110  3460  3810  3910  4390  50020 5010  5020  50290
                          50300 5090  5410  5600  7010  80080 8010  8020  8030  8040  8050  8060  8070  8080  9010 
                          9340  6050  6060  6070  6080
                        }
  nationalprovinces   = { 1475 1480 1481 1482 1483 1494 1592 1731 1474 }
landunit = {
	name = "17th Infantry Division"
	id = { type = 24571 id = 1 }
	location = 1731
	division = {
	id = { type = 24571 id = 2 }
	name = "1th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 3 }
	name = "2th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 4 }
	name = "3th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 5 }
	name = "4th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 6 }
	name = "5th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 7 }
	name = "6th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "37th Infantry Division"
	id = { type = 24571 id = 8 }
	location = 1731
	division = {
	id = { type = 24571 id = 9 }
	name = "7th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 10 }
	name = "8th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 11 }
	name = "9th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 12 }
	name = "10th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 13 }
	name = "11th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 14 }
	name = "12th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "14th Infantry Division"
	id = { type = 24571 id = 15 }
	location = 1731
	division = {
	id = { type = 24571 id = 16 }
	name = "13th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 17 }
	name = "14th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 18 }
	name = "15th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 19 }
	name = "16th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 20 }
	name = "17th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 21 }
	name = "18th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "40th Infantry Division"
	id = { type = 24571 id = 22 }
	location = 1731
	division = {
	id = { type = 24571 id = 23 }
	name = "19th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 24 }
	name = "20th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 25 }
	name = "21th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 26 }
	name = "22th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 27 }
	name = "23th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 28 }
	name = "24th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "10th Infantry Division"
	id = { type = 24571 id = 29 }
	location = 1480
	division = {
	id = { type = 24571 id = 30 }
	name = "25th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 31 }
	name = "26th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 32 }
	name = "27th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 33 }
	name = "28th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 34 }
	name = "29th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 35 }
	name = "30th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "11th Infantry Division"
	id = { type = 24571 id = 36 }
	location = 1480
	division = {
	id = { type = 24571 id = 37 }
	name = "31th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 38 }
	name = "32th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 39 }
	name = "33th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 40 }
	name = "34th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 41 }
	name = "35th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 42 }
	name = "36th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "8th Infantry Division"
	id = { type = 24571 id = 43 }
	location = 1480
	division = {
	id = { type = 24571 id = 44 }
	name = "37th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 45 }
	name = "38th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 46 }
	name = "39th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 47 }
	name = "40th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 48 }
	name = "41th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 49 }
	name = "42th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "15th Infantry Division"
	id = { type = 24571 id = 50 }
	location = 1592
	division = {
	id = { type = 24571 id = 51 }
	name = "43th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 52 }
	name = "44th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 53 }
	name = "45th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 54 }
	name = "46th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 55 }
	name = "47th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 56 }
	name = "48th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "35th Infantry Division"
	id = { type = 24571 id = 57 }
	location = 1475
	division = {
	id = { type = 24571 id = 58 }
	name = "49th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 59 }
	name = "50th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 60 }
	name = "51th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 61 }
	name = "52th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 62 }
	name = "53th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 63 }
	name = "54th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "16th Infantry Division"
	id = { type = 24571 id = 64 }
	location = 1482
	division = {
	id = { type = 24571 id = 65 }
	name = "55th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 66 }
	name = "56th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 67 }
	name = "57th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 68 }
	name = "58th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 69 }
	name = "59th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 70 }
	name = "60th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "18th Infantry Division"
	id = { type = 24571 id = 71 }
	location = 1474
	division = {
	id = { type = 24571 id = 72 }
	name = "61th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 73 }
	name = "62th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 74 }
	name = "63th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 75 }
	name = "64th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 76 }
	name = "65th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 77 }
	name = "66th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "12th Infantry Division"
	id = { type = 24571 id = 78 }
	location = 1494
	division = {
	id = { type = 24571 id = 79 }
	name = "67th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 80 }
	name = "68th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 81 }
	name = "69th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 82 }
	name = "70th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 83 }
	name = "71th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 84 }
	name = "72th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "19th Infantry Division"
	id = { type = 24571 id = 85 }
	location = 1483
	division = {
	id = { type = 24571 id = 86 }
	name = "73th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 87 }
	name = "74th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 88 }
	name = "75th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 89 }
	name = "76th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 90 }
	name = "77th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 91 }
	name = "78th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "23th Infantry Division"
	id = { type = 24571 id = 92 }
	location = 1592
	division = {
	id = { type = 24571 id = 93 }
	name = "79th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 94 }
	name = "80th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 95 }
	name = "81th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 96 }
	name = "82th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 97 }
	name = "83th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 98 }
	name = "84th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "7th Infantry Division"
	id = { type = 24571 id = 99 }
	location = 1475
	division = {
	id = { type = 24571 id = 100 }
	name = "85th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 101 }
	name = "86th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 102 }
	name = "87th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 103 }
	name = "88th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 104 }
	name = "89th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 105 }
	name = "90th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "9th Infantry Division"
	id = { type = 24571 id = 106 }
	location = 1482
	division = {
	id = { type = 24571 id = 107 }
	name = "91th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 108 }
	name = "92th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 109 }
	name = "93th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 110 }
	name = "94th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 111 }
	name = "95th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 112 }
	name = "96th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "33th Infantry Division"
	id = { type = 24571 id = 113 }
	location = 1474
	division = {
	id = { type = 24571 id = 114 }
	name = "97th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 115 }
	name = "98th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 116 }
	name = "99th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 117 }
	name = "100th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 118 }
	name = "101th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 119 }
	name = "102th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "41th Infantry Division"
	id = { type = 24571 id = 120 }
	location = 1731
	division = {
	id = { type = 24571 id = 121 }
	name = "103th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 122 }
	name = "104th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 123 }
	name = "105th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 124 }
	name = "106th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 125 }
	name = "107th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24571 id = 126 }
	name = "108th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
  }
landunit = {
	name = "6th Armored Division"
	id = { type = 24571 id = 127 }
	location = 1731
	division = {
	id = { type = 24571 id = 128 }
	name = "1th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 129 }
	name = "2th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 130 }
	name = "3th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 131 }
	name = "1th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 132 }
	name = "2th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 133 }
	name = "3th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
}
landunit = {
	name = "1th Armored Division"
	id = { type = 24571 id = 134 }
	location = 1731
	division = {
	id = { type = 24571 id = 135 }
	name = "4th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 136 }
	name = "5th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 137 }
	name = "6th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 138 }
	name = "4th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 139 }
	name = "5th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 140 }
	name = "6th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
  }
landunit = {
	name = "26th Armored Division"
	id = { type = 24571 id = 141 }
	location = 1480
	division = {
	id = { type = 24571 id = 142 }
	name = "7th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 143 }
	name = "8th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 144 }
	name = "9th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 145 }
	name = "10th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 146 }
	name = "11th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 147 }
	name = "12th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
}
landunit = {
	name = "25th Armored Division"
	id = { type = 24571 id = 148 }
	location = 1731
	division = {
	id = { type = 24571 id = 149 }
	name = "13th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 150 }
	name = "14th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 151 }
	name = "15th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 152 }
	name = "16th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 153 }
	name = "17th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24571 id = 154 }
	name = "18th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
  }
}
