
##############################
# Country definition for U01 #
##############################

province = { id = 900 anti_air = 2 ic = 6 } # ポートサイド
province = { id = 901 anti_air = 2 oilpool = 1000 supplypool = 2000 ic = 4 } # スエズ
province = { id = 902 ic = 3 } # ディムヤート
province =
{ id       = 903
  anti_air = 2
  ic       = 3
  air_base = { size = 4 current_size = 4 }
}            # カイロ
province =
{ id         = 907
  anti_air   = 5
  ic         = 6
  air_base   = { size = 4 current_size = 4 }
  naval_base = { size = 10 current_size = 10 }
}              # アレクサンドリア
province = { id = 911 } # シディ・アブドラ・ラマン
province =
{ id       = 914
  air_base = { size = 2 current_size = 2 }
  ic       = 2
}            # マルサマトルーフ
province =
{ id            = 920
  anti_air      = 2
  ic            = 7
  naval_base    = { size = 4 current_size = 4 }
  air_base      = { size = 4 current_size = 4 }
  landfort      = 3
}                 # トブルク
province =
{ id            = 924
  anti_air      = 1
  ic            = 6
  naval_base    = { size = 1 current_size = 1 }
  air_base      = { size = 4 current_size = 4 }
}                 # ベンガジ
province = { id = 929 ic = 1 } # サート
province =
{ id            = 932
  anti_air      = 1
  ic            = 6
  naval_base    = { size = 2 current_size = 2 }
  air_base      = { size = 10 current_size = 10 }
  oilpool       = 100
  supplypool    = 500
}                 # トリポリ
province =
{ id            = 939
  anti_air      = 4
  ic            = 5
  air_base      = { size = 4 current_size = 4 }
  naval_base    = { size = 4 current_size = 4 }
}                 # チュニス
province =
{ id         = 940
  anti_air   = 1
  naval_base = { size = 2 current_size = 2 }
}              # ビゼルト
province = { id = 945 ic = 2 } # アンナバ
province =
{ id            = 949
  anti_air      = 2
  ic            = 5
  air_base      = { size = 2 current_size = 2 }
}                 # アルジェ
province =
{ id         = 950
  anti_air   = 4
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 4 current_size = 4 }
  supplypool = 2000
  oilpool    = 500
}              # オラン
province = { id = 958 ic = 5 } # ラバト
province =
{ id         = 960
  anti_air   = 3
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 4 current_size = 4 }
}              # カサブランカ
province = { id = 966 ic = 2 } # マラケシュ
province = { id = 971 ic = 1 } # エルアイウン
province =
{ id            = 981
  anti_air      = 3
  coastalfort   = 1
  air_base      = { size = 2 current_size = 2 }
  naval_base    = { size = 4 current_size = 4 }
  ic            = 5
}                 # ダカール
province = { id = 987 ic = 2 } # コナクリ
province =
{ id         = 988
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 2 current_size = 2 }
  oilpool    = 100
  supplypool = 500
}              # フリータウン
province = { id = 992 ic = 4 } # モンロヴィア
province =
{ id         = 993
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 1 current_size = 1 }
}              # アビジャン
province =
{ id         = 1002
  anti_air   = 2
  naval_base = { size = 1 current_size = 1 }
  air_base   = { size = 2 current_size = 2 }
  ic         = 2
}              # アクラ
province = { id = 1006 ic = 3 } # コトヌー
province =
{ id         = 1008
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 2 current_size = 2 }
  oilpool    = 100
  supplypool = 500
}              # ラゴス
province = { id = 1011 ic = 2 } # ポートハーコート
province = { id = 1030 ic = 2 } # ポートスーダン
province =
{ id         = 1037
  anti_air   = 2
  naval_base = { size = 2 current_size = 2 }
  supplypool = 500
  oilpool    = 100
  ic         = 2
}              # ジブチ
province = { id = 1038 ic = 2 } # アッサブ
province = { id = 1046 ic = 8 } # アディスアベバ
province = { id = 1056 ic = 1 } # モガディシュ
province =
{ id         = 1061
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 1 current_size = 1 }
}              # モンバサ
province =
{ id       = 1066
  anti_air = 1
  air_base = { size = 2 current_size = 2 }
}            # ハルツーム
province =
{ id         = 1089
  anti_air   = 1
  naval_base = { size = 1 current_size = 1 }
}              # ヤウンデ
province =
{ id            = 1093
  anti_air      = 1
  naval_base    = { size = 1 current_size = 1 }
}                 # リーブルヴィル
province = { id = 1095 ic = 4 } # マタディ
province = { id = 1096 ic = 3 } # ロアンダ
province = { id = 1098 ic = 2 } # ルルアブール
province = { id = 1100 ic = 3 } # フアンボ
province = { id = 1134 ic = 2 } # アンタナナリボ
province =
{ id          = 1137
  anti_air    = 2
  coastalfort = 1
  air_base    = { size = 2 current_size = 2 }
  naval_base  = { size = 4 current_size = 4 }
  supplypool  = 500
  oilpool     = 100
  ic          = 3
}               # タマタブ
province = { id = 1144 } # エリザベートヴィル
province = { id = 1800 ic = 3 } # エルアリシュ
province = { id = 1801 ic = 1 } # シャルムエルシェイク
province = { id = 2332 ic = 3 } # ナドル

#####################
# Country main data #
#####################

country =
{ tag                 = U01
  manpower            = 650
  energy              = 18000
  metal               = 20000
  rare_materials      = 15000
  oil                 = 18000
  supplies            = 80000
  money               = 1100
  capital             = 907
  regular_id          = U06
  transports          = 800
  escorts             = 600
  ##nationalidentity    = "policy_identity_defensive"
  ##socialpolicy        = "policy_social_ethnic"
  #nationalculture     = "policy_culture_ethnic"
  policy              = { democratic = 3 political_left = 3 freedom = 2 free_market = 8 professional_army = 9 defense_lobby = 8 interventionism = 9 }
  ownedprovinces      = { }
  controlledprovinces = { }
  techapps            = { 10050 10060 10070 1010  10150 10190 1020  10200 10210 1030  1040  10490 1110  1120  1130  1210  1220  1230  1530  1550 
                          1560  1570  1580  1650  1660  1670  1750  1760  1850  1860  1950  2010  2020  20210 2030  20310 20410 2610  2910  3010 
                          3020  3030  3110  3120  3130  3210  3220  3460  3470  3480  3810  3820  3830  3910  3920  3940  40100 4390  4400  4410 
                          4420  4480  4490  4560  4570  50020 50030 50040 50070 50080 50090 5010  50100 50110 50120 5020  50290 5030  50300 50310
                          5090  5100  5110  5170  5180  5190  5250  5260  5330  5340  5410  5420  5430  5460  5490  5600  5610  5620  5910  5920 
                          5930  6010  6020  6050  6420  6430  6440  6450  6460  7010  80080 80090 8010  8020  8030  8040  8050  8060  8070  8880 
                          8890  8900  8910  8920  9010  9020  9030  9040  9050  9060  9190  9200  9210  9220  9340  9350  9360  9370  9380  9390 
                          9520  9670  9680  9690  9700  9710  9840  9850  9860 
                        }
  nationalprovinces   = { 1044 1048 1049 1042 1043 1050 1045 1046 1047 1053 1052 1051 1054 2342 1800 1801 900  901  915  914  913  912  911  910 
                          909  907  906  905  904  903  902  4    1020 1021 1023 1024 1025 1026 1027 916  917  919  920  921  922  923  924  925  926 
                          927  928  929  930  931  932  933  952  1017 942  943  944  945  946  947  948  949  950  951  953  1013 956  958  959  960 
                          966  968  969  1121 2331 976  977  978  979  980  985  986  994  973  974  998  1083 1084 1071 1072 1073 1074 1095 1098 1127
                          1142 1143 1144 1097 1063 2283 1028 1029 1030 1031 1032 1040 1041 1065 1066 1067 1068 1069 2334 999  1007 1008 1009 1010 1085
                          1145 1011 1012 1126 1137 1038 1058 1140 1139 1062 1141 1061 1060 1064 2284 1059 1134 1033 1055 1124 1111 1105 1112 1131 1128
                          1132 1133 1138 2024 1039 1056 1102 1103 1100 1104 1099 1129 1125 1130 1123 1816 1035 1090 2339 1096 1101 1089 1077 1091 1092
                          1037 1094 1093 1075 1076 1086 1087 1078 1088 940  939  941  937  938  934  936  935  2332 955  967  1034 1080 971  972  981 
                          1081 982  984  983  1006 987  996  988  1003 991  992  993  1001 995  1002 1004 1000 997  1005 1146 990  1079 1070
                        }
  landunit =
  { id       = { type = 24501 id = 5123 }
    name     = "1th Corps"
    location = 958
    home     = 958
    #mission  = { type = none target = 958 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5125 }
      name          = "1th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5126 }
    name     = "2th Corps"
    location = 981
    home     = 981
    #mission  = { type = none target = 981 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5128 }
      name          = "2th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5129 }
    name     = "3th Corps"
    location = 992
    home     = 992
    #mission  = { type = none target = 992 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5131 }
      name          = "3th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5132 }
    name     = "4th Corps"
    location = 1093
    home     = 1093
    #mission  = { type = none target = 1093 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5134 }
      name          = "4th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5135 }
    name     = "5th Corps"
    location = 1095
    home     = 1095
    #mission  = { type = none target = 1095 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5137 }
      name          = "5th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5138 }
    name     = "6th Corps"
    location = 1096
    home     = 1096
    #mission  = { type = none target = 1096 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5140 }
      name          = "6th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5141 }
    name     = "7th Corps"
    location = 1037
    home     = 1037
    #mission  = { type = none target = 1037 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5143 }
      name          = "7th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5144 }
    name     = "8th Corps"
    location = 1046
    home     = 1046
    #mission  = { type = none target = 1046 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5146 }
      name          = "8th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5147 }
    name     = "9th Corps"
    location = 939
    home     = 939
    #mission  = { type = none target = 939 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5149 }
      name          = "9th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5150 }
    name     = "10th Corps"
    location = 949
    home     = 949
    #mission  = { type = none target = 949 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5152 }
      name          = "10th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5153 }
    name     = "11th Corps"
    location = 932
    home     = 932
    #mission  = { type = none target = 932 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5155 }
      name          = "11th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5156 }
    name     = "12th Corps"
    location = 924
    home     = 924
    #mission  = { type = none target = 924 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5158 }
      name          = "12th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5159 }
    name     = "13th Corps"
    location = 920
    home     = 920
    #mission  = { type = none target = 920 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5161 }
      name          = "13th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5162 }
    name     = "14th Corps"
    location = 907
    home     = 907
    #mission  = { type = none target = 907 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5164 }
      name          = "14th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5165 }
    name     = "15th Corps"
    location = 903
    home     = 903
    #mission  = { type = none target = 903 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5167 }
      name          = "15th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5168 }
    name     = "16th Corps"
    location = 1134
    home     = 1134
    #mission  = { type = none target = 1134 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5170 }
      name          = "16th Support Unit"
      model         = 2
      brigade_model = 0
      type          = garrison
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5171 }
    name     = "Middle East Region HQ"
    location = 1803
    home     = 1803
    #mission  = { type = none target = 1803 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5173 }
      name          = "1th Military Headquarters"
      model         = 2
      brigade_model = 0
      type          = hq
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5174 }
    name     = "17th Corps"
    location = 1796
    home     = 1796
    #mission  = { type = none target = 1796 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5176 }
      name          = "1th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5177 }
      name          = "1th Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5178 }
      name          = "2th Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5179 }
    name     = "18th Corps"
    location = 1803
    home     = 1803
    #mission  = { type = none target = 1803 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5181 }
      name          = "1th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5182 }
      name          = "2th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5183 }
      name          = "3th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5184 }
    name     = "19th Corps"
    location = 1882
    home     = 1882
    #mission  = { type = none target = 1882 #missionscope = 0 percentage = 0.0000 night = yes day = yes task = 1 location = 1795 }
    movetime = { year = 1990 month = january day = 2 hour = 9 }
    movement = { 1795 1794 }
    division =
    { id            = { type = 24501 id = 5186 }
      name          = "4th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5187 }
      name          = "5th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5188 }
      name          = "6th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5189 }
    name     = "20th Corps"
    location = 1796
    home     = 1796
    #mission  = { type = none target = 1796 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5191 }
      name          = "1th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5192 }
      name          = "2th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5193 }
      name          = "2th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5194 }
    name     = "21th Corps"
    location = 1795
    home     = 1795
    #mission  = { type = none target = 1795 #missionscope = 0 percentage = 0.0000 night = yes day = yes task = 1 location = 1794 }
    movetime = { year = 1990 month = january day = 1 hour = 10 }
    movement = { 1794 }
    division =
    { id            = { type = 24501 id = 5196 }
      name          = "3th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5197 }
      name          = "4th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5198 }
      name          = "3th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5199 }
    name     = "Suez Military Headquarters"
    location = 903
    home     = 903
    #mission  = { type = none target = 903 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5201 }
      name          = "2th Military Headquarters"
      model         = 2
      brigade_model = 0
      type          = hq
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5202 }
    name     = "22th Corps"
    location = 901
    home     = 901
    #mission  = { type = none target = 901 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5204 }
      name          = "5th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5205 }
      name          = "6th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5206 }
      name          = "4th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5207 }
    name     = "23th Corps"
    location = 900
    home     = 900
    #mission  = { type = none target = 900 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5209 }
      name          = "7th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5210 }
      name          = "8th Tactical Armored Batallion"
      model         = 3
      brigade_model = 0
      type          = paratrooper
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5211 }
      name          = "5th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5212 }
    name     = "24th Corps"
    location = 901
    home     = 901
    #mission  = { type = none target = 901 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5214 }
      name          = "6th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5215 }
      name          = "7th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5216 }
      name          = "8th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5217 }
    name     = "25th Corps"
    location = 1800
    home     = 1800
    #mission  = { type = none target = 1800 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5219 }
      name          = "9th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5220 }
      name          = "3th Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5221 }
      name          = "4th Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5222 }
    name     = "26th Corps"
    location = 900
    home     = 900
    #mission  = { type = none target = 900 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5224 }
      name          = "7th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5225 }
      name          = "8th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5226 }
      name          = "9th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5227 }
    name     = "27th Corps"
    location = 901
    home     = 901
    #mission  = { type = none target = 901 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5229 }
      name          = "10th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5230 }
      name          = "11th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5231 }
      name          = "12th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5232 }
    name     = "28th Corps"
    location = 900
    home     = 900
    #mission  = { type = none target = 900 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5234 }
      name          = "13th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5235 }
      name          = "14th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5236 }
      name          = "15th Mechanized Armored Infantry Unit"
      model         = 2
      brigade_model = 0
      type          = mechanized
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5237 }
    name     = "29th Corps"
    location = 901
    home     = 901
    #mission  = { type = none target = 901 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5239 }
      name          = "10th Cavalry"
      model         = 3
      brigade_model = 0
      type          = armor
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5240 }
      name          = "5th Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5241 }
      name          = "6th Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
  }
  landunit =
  { id       = { type = 24501 id = 5242 }
    name     = "30th Corps"
    location = 1800
    home     = 1800
    #mission  = { type = none target = 1800 #missionscope = 0 percentage = 0.0000 night = yes day = yes }
    division =
    { id            = { type = 24501 id = 5244 }
      name          = "7th Armored Corps"
      model         = 3
      brigade_model = 0
      type          = light_armor
      extra         = artillery
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5245 }
      name          = "8th Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
    division =
    { id            = { type = 24501 id = 5246 }
      name          = "9th Armored Corps"
      model         = 2
      brigade_model = 0
      type          = light_armor
      strength      = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6182 }
    name     = "African Navy 3th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5251 }
      name     = "1th Missile Boat Division"
      model    = 3
      type     = light_cruiser
      strength            = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6184 }
    name     = "African Navy 4th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5261 }
      name     = "2th Destroyer Division"
      model    = 3
      type     = destroyer
      strength            = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6186 }
    name     = "African Navy 5th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5262 }
      name     = "3th Destroyer Division"
      model    = 2
      type     = destroyer
      strength            = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6188 }
    name     = "African Navy 6th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5263 }
      name     = "4th Destroyer Division"
      model    = 2
      type     = destroyer
      strength            = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6190 }
    name     = "African Navy 7th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5264 }
      name     = "5th Destroyer Division"
      model    = 2
      type     = destroyer
      strength            = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6192 }
    name     = "African Navy 8th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5266 }
      name     = "7th Destroyer Division"
      model    = 2
      type     = destroyer
      strength            = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6194 }
    name     = "African Navy 9th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5270 }
      name     = "1th Transport Division"
      model    = 4
      type     = transport
      strength            = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6196 }
    name     = "African Navy 10th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5271 }
      name     = "2th Transport Division"
      model    = 4
      type     = transport
      strength            = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 6198 }
    name     = "African Navy 11th Fleet"
    location = 1002
    home     = 1002
    base     = 1002
    division =
    { id       = { type = 24502 id = 5272 }
      name     = "3th Transport Division"
      model    = 4
      type     = transport
      strength            = 100
    }
  }
  navalunit =
  { id       = { type = 24502 id = 2527 }
    name     = "African Navy 1th Fleet"
    location = 907
    home     = 907
    base     = 907
    division =
    { id       = { type = 24502 id = 5247 }
      name     = "1th Cruiser Division"
      model    = 3
      type     = heavy_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5248 }
      name     = "2th Cruiser Division"
      model    = 3
      type     = heavy_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5249 }
      name     = "3th Cruiser Division"
      model    = 2
      type     = heavy_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5250 }
      name     = "4th Cruiser Division"
      model    = 2
      type     = heavy_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5252 }
      name     = "2th Missile Boat Division"
      model    = 3
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5253 }
      name     = "3th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5254 }
      name     = "4th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5255 }
      name     = "5th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5256 }
      name     = "6th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5257 }
      name     = "7th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5258 }
      name     = "8th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5259 }
      name     = "9th Missile Boat Division"
      model    = 2
      type     = light_cruiser
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5260 }
      name     = "1th Destroyer Division"
      model    = 3
      type     = destroyer
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5265 }
      name     = "6th Destroyer Division"
      model    = 2
      type     = destroyer
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5267 }
      name     = "8th Destroyer Division"
      model    = 2
      type     = destroyer
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5268 }
      name     = "9th Destroyer Division"
      model    = 2
      type     = destroyer
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5269 }
      name     = "10th Destroyer Division"
      model    = 2
      type     = destroyer
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5273 }
      name     = "1th Submarine Fleet"
      model    = 2
      type     = submarine
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5274 }
      name     = "2th Submarine Fleet"
      model    = 2
      type     = submarine
      strength            = 100
    }
    division =
    { id       = { type = 24502 id = 5275 }
      name     = "3th Submarine Fleet"
      model    = 2
      type     = submarine
      strength            = 100
    }
  }
}
