
##############################
# Country definition for U12 #
##############################

province = { id = 1431 air_base = 10 infra = 2.0 landfort = 10 anti_air = 10 } #

#####################
# Country main data #
#####################

country =
{ tag                 = U12
  manpower            = 99999
  energy              = 900000
  metal               = 900000
  rare_materials      = 900000
  oil                 = 900000
  supplies            = 900000
  money               = 900000
  capital             = 1431
  transports          = 9999
  escorts             = 9999
  extra_tc            = 9999
  policy =
  { democratic        = 1
    political_left    = 1
    freedom           = 1
    free_market       = 1
    professional_army = 1
    defense_lobby     = 10
    interventionism   = 10
  }
  nationalprovinces   = { 1431 }
  ownedprovinces      = { 1431 }
  controlledprovinces = { 1431 }
  techapps            = { 7020 }
  free                = { energy = 1000 manpower = 9 metal = 1000 rare_materials = 1000 oil = 1000 money = 1000 ic = 500 supplies = 1000 }
  landunit =
  { id       = { type = 15001 id = 5177 }
    name     = "1st Corps"
    location = 1431
    division =
    { id            = { type = 15001 id = 5179 }
      name          = "1th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = heavy_armor
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5180 }
    name     = "2nd Corps"
    location = 1431
    division =
    { id            = { type = 15001 id = 5182 }
      name          = "2th BETA Group"
      model         = 33
      type          = hq
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5183 }
    name     = "3rd Corps"
    location = 1431
    division =
    { id            = { type = 15001 id = 5185 }
      name          = "3th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = light_armor_brigade
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5186 }
    name     = "4th Corps"
    location = 1431
    division =
    { id            = { type = 15001 id = 5188 }
      name          = "1th BETA Group"
      model         = 33
      type          = marine
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5189 }
      name          = "1th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5190 }
      name          = "1th BETA Group"
      model         = 33
      type          = infantry
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5191 }
      name          = "2th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5192 }
      name          = "3th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5193 }
    name     = "5th Corps"
    location = 1431
    division =
    { id            = { type = 15001 id = 5195 }
      name          = "2th BETA Group"
      model         = 33
      type          = marine
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5196 }
      name          = "2th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5197 }
      name          = "4th BETA Group"
      model         = 33
      type          = infantry
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5198 }
      name          = "5th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5199 }
      name          = "6th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5200 }
    name     = "6th Corps"
    location = 1431
    division =
    { id            = { type = 15001 id = 5202 }
      name          = "3th BETA Group"
      model         = 33
      type          = marine
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5203 }
      name          = "3th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5204 }
      name          = "7th BETA Group"
      model         = 33
      type          = infantry
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5205 }
      name          = "8th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5206 }
      name          = "9th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5207 }
    name     = "7th Corps"
    location = 1431
    division =
    { id            = { type = 15001 id = 5209 }
      name          = "4th BETA Group"
      model         = 33
      type          = marine
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5210 }
      name          = "4th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5211 }
      name          = "10th BETA Group"
      model         = 33
      type          = infantry
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5212 }
      name          = "11th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5213 }
      name          = "12th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5214 }
    name     = "8th Corps"
    location = 1431
    division =
    { id            = { type = 15001 id = 5216 }
      name          = "5th BETA Group"
      model         = 33
      type          = marine
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5217 }
      name          = "5th BETA Group"
      model         = 33
      brigade_model = 0
      type          = bergsjaeger
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5218 }
      name          = "13th BETA Group"
      model         = 33
      type          = infantry
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5219 }
      name          = "14th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = heavy_armor
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5220}
      name          = "15th BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5250}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5251}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5252}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5253}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5254}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5255}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5256}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5257}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5258}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5259}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5260}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5261}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5262}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5263}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5264}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5265}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5266}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5267}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5268}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5269}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5270}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5271}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5272}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5273}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5274}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5275}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5276}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5277}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5278}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5279}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5280}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5281}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5282}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5283}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5284}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5285}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5286}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5287}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5288}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5289}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5290}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5291}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5292}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5293}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5294}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5295}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5296}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5297}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5298}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5299}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5300}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5301}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5302}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5303}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5304}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5305}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5306}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5307}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5308}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5309}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5310}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5311}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5312}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5313}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5314}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5315}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5316}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5317}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5318}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5319}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5320}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5321}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5322}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5323}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5324}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5325}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5326}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5327}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5328}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5329}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5330}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5331}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5332}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5333}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5334}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5335}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5336}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5337}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5338}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5339}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5340}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5341}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5342}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5343}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5344}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5345}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5346}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5347}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5348}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5349}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
    division =
    { id            = { type = 15001 id = 5350}
      name          = "BETA Group"
      model         = 33
      brigade_model = 0
      type          = infantry
      extra         = light_armor_brigade
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5221 }
    name     = "9th Corps"
    location = 1431
    division =
    { id            = { type = 15001 id = 5223 }
      name          = "4th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = heavy_armor
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5224 }
    name     = "10th Corps"
    location = 1431
    home     = 1431
    division =
    { id            = { type = 15001 id = 5226 }
      name          = "5th BETA Group"
      model         = 33
      type          = hq
      strength      = 100.0000
    }
  }
  landunit =
  { id       = { type = 15001 id = 5227 }
    name     = "11th Corps"
    location = 1431
    home     = 1431
    division =
    { id            = { type = 15001 id = 5229 }
      name          = "6th BETA Group"
      model         = 33
      brigade_model = 0
      type          = hq
      extra         = light_armor_brigade
      strength      = 100.0000
    }
  }
    landunit =
    { id       = { type = 15001 id = 1 }
      name     = "Kashgar Hive"
      location = 1431
      division =
      { type   = garrison
        model  = 11
        id     = { type = 15004 id = 1 }
        name   = "Kashgar Hive"
        locked = yes
      }
      division =
      { type   = garrison
        model  = 33
        id     = { type = 15004 id = 2 }
        name   = "Gate-Class"
        locked = yes
      }
    }
}
