
##############################
# Country definition for POL #
##############################

#####################
# Country main data #
#####################

country =
{ tag                 = POL
  manpower            = 4250
  energy              = 58000
  metal               = 36000
  rare_materials      = 45000
  oil                 = 12000
  supplies            = 8000
  money               = 3500
  capital             = 75
  transports          = 1800
  escorts             = 1500
  regular_id          = U06
  nationalprovinces   = { 208 211 222 305 306 479 480 481 483 484 485 486 487 488 489 504 505 508 1875 1876 1884 308 1400 473 482 303 304 307 1811 302 1779 1810 1623 1595 }
  ownedprovinces      = { 208 211 222 305 306 479 480 481 483 484 485 486 487 488 489 504 505 508 1875 1876 1884 308 1400 473 482 303 304 307 1811 302 1779 1810 1623 1595 }
  controlledprovinces = { 208 211 222 305 306 479 480 481 483 484 485 486 487 488 489 504 505 508 1875 1876 1884 308 1400 473 482 303 304 307 1811 302 1779 1810 1623 1595 }
  techapps            = { 10050 10060 1010  10190 1020  10200 1030    1110  1120 
                          1210  1220  1470  1490  1510  1530  1550  1560  1650  1660
                          1750  1760  1850  1860  1950  1960
                          3010  3020  30410 30420 30510 30520 3110 
                          3120  3210  3220  3460  3470
                          3560      3910  3920
                          3940  40100 40110 4390  4400  4010  4020  4110  4120  4210  4220  4640  4650  4820  4830
                          4560  50020 50030   50070 50080  5010    50120 5020  50290 50300 50310
                          5090  5100  5170  5180  5250  5260  5330  5340  5410  5420
                          5490  5500  5600  5610  5680  5690  5760  5770  5780  5790  5800  5810
                          5910  5920  7010  80080 80090 8010  80100 80160 8020  8030  8040  8050  8060  8070  8280  8290  8300  8310  8320  8330  9010  90140
                          90190 9020  9030  9040  9050  9060  9070  9190  9200  9210  9340  9350  9360  9370  9380  9390  9400  9520  9530  9540 
                          9550  9670  9680  9690  9700  9710  9720  9840  9850  9860  9990  6000  6010  6020  6030  6040
                        }
  inventions          = { }
  policy              = { democratic = 2 political_left = 9 freedom = 2 free_market = 2 professional_army = 9 defense_lobby = 10 interventionism = 9 }
landunit = {
	name = "1th Tank Division"
	id = { type = 19521 id = 1 }
	location = 302
	division = {
	id = { type = 19521 id = 2 }
	name = "1th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 3 }
	name = "2th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 4 }
	name = "3th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
  }
landunit = {
	name = "2th Tank Division"
	id = { type = 19521 id = 5 }
	location = 308
	division = {
	id = { type = 19521 id = 6 }
	name = "4th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 7 }
	name = "5th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 8 }
	name = "6th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
  }
landunit = {
	name = "3th Tank Division"
	id = { type = 19521 id = 9 }
	location = 1400
	division = {
	id = { type = 19521 id = 10 }
	name = "7th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 11 }
	name = "8th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 12 }
	name = "9th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
  }
landunit = {
	name = "4th Tank Division"
	id = { type = 19521 id = 13 }
	location = 485
	division = {
	id = { type = 19521 id = 14 }
	name = "10th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 15 }
	name = "11th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 16 }
	name = "12th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
  }
landunit = {
	name = "5th Tank Division"
	id = { type = 19521 id = 17 }
	location = 306
	division = {
	id = { type = 19521 id = 18 }
	name = "13th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 19 }
	name = "14th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 19521 id = 20 }
	name = "15th Tank Regiment"
	type = light_armor
	model = 11
	experience = 0
        #locked = yes
	}
  }
landunit = {
	name = "1th Motorized Sniper Division"
	id = { type = 19521 id = 205 }
	location = 302
	division = {
	id = { type = 19521 id = 206 }
	name = "1th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 207 }
	name = "2th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 208 }
	name = "3th Motorized Sniper Regiment"
	type = mechanized
	model = 1
	experience = 0
	#locked = yes
	}
  }
landunit = {
	name = "2th Motorized Sniper Division"
	id = { type = 19521 id = 209 }
	location = 302
	division = {
	id = { type = 19521 id = 210 }
	name = "4th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 211 }
	name = "5th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 212 }
	name = "6th Motorized Sniper Regiment"
	type = mechanized
	model = 1
	experience = 0
	#locked = yes
	}
}
landunit = {
	name = "3th Motorized Sniper Division"
	id = { type = 19521 id = 213 }
	location = 308
	division = {
	id = { type = 19521 id = 214 }
	name = "7th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 215 }
	name = "8th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 216 }
	name = "9th Motorized Sniper Regiment"
	type = mechanized
	model = 1
	experience = 0
	#locked = yes
	}
}
landunit = {
	name = "4th Motorized Sniper Division"
	id = { type = 19521 id = 217 }
	location = 308
	division = {
	id = { type = 19521 id = 218 }
	name = "10th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 219 }
	name = "11th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 220 }
	name = "12th Motorized Sniper Regiment"
	type = mechanized
	model = 1
	experience = 0
	#locked = yes
	}
}
landunit = {
	name = "5th Motorized Sniper Division"
	id = { type = 19521 id = 221 }
	location = 1400
	division = {
	id = { type = 19521 id = 222 }
	name = "13th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 223 }
	name = "14th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 224 }
	name = "15th Motorized Sniper Regiment"
	type = mechanized
	model = 1
	experience = 0
	#locked = yes
	}
}
landunit = {
	name = "6th Motorized Sniper Division"
	id = { type = 19521 id = 225 }
	location = 306
	division = {
	id = { type = 19521 id = 226 }
	name = "16th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 227 }
	name = "17th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 228 }
	name = "18th Motorized Sniper Regiment"
	type = mechanized
	model = 1
	experience = 0
	#locked = yes
	}
}
landunit = {
	name = "7th Motorized Sniper Division"
	id = { type = 19521 id = 229 }
	location = 479
	division = {
	id = { type = 19521 id = 230 }
	name = "19th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 231 }
	name = "20th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 232 }
	name = "21th Motorized Sniper Regiment"
	type = mechanized
	model = 1
	experience = 0
	#locked = yes
	}
}
landunit = {
	name = "8th Motorized Sniper Division"
	id = { type = 19521 id = 233 }
	location = 485
	division = {
	id = { type = 19521 id = 234 }
	name = "22th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 235 }
	name = "23th Motorized Sniper Regiment"
	type = light_armor
	model = 11
	experience = 0
	#locked = yes
	}
	division = {
	id = { type = 19521 id = 236 }
	name = "24th Motorized Sniper Regiment"
	type = mechanized
	model = 1
	experience = 0
	#locked = yes
	}
}
}
