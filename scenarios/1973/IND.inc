
##############################
# Country definition for IND #
##############################

province =
{ id       = 631
  anti_air = 1
  air_base = { size = 2 current_size = 2 }
}            # インドール
province = { id = 635 ic = 2 } # ラーイガル
province = { id = 669 ic = 3 } # ブルハーンプル
province =
{ id       = 702
  air_base = { size = 2 current_size = 2 }
}            # ラーヤチュール
province =
{ id       = 773
  air_base = { size = 2 current_size = 2 }
}            # コインバトール
province =
{ id       = 781
  air_base = { size = 2 current_size = 2 }
}            # アディラーバード
province =
{ id       = 791
  air_base = { size = 2 current_size = 2 }
}            # バランギル
province =
{ id         = 1457
  anti_air   = 2
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 4 current_size = 4 }
  ic         = 2
}              # カルカッタ
province = { id = 1458 ic = 3 } # ブヴァネーシュヴァル
province = { id = 1459 ic = 2 } # ランチ
province =
{ id       = 1462
  air_base = { size = 5 current_size = 5 }
}            # アマラヴァティー
province =
{ id       = 1469
  anti_air = 2
  air_base = { size = 4 current_size = 4 }
}            # デリー
province =
{ id            = 1505
  anti_air      = 3
  air_base      = { size = 2 current_size = 2 }
  naval_base    = { size = 2 current_size = 2 }
  ic            = 4
}                 # ボンベイ
province = { id = 1507 ic = 3 } # ハイデラバード
province = { id = 1508 ic = 2 } # ヴィシャーカパトナム
province = { id = 1509 ic = 3 } # ネッロール
province =
{ id         = 1513
  naval_base = { size = 4 current_size = 4 }
  air_base   = { size = 2 current_size = 2 }
}              # マンガロール
province = { id = 1515 ic = 2 } # マドゥライ
province = { id = 1516 ic = 1 } # キャンディ
province =
{ id         = 1517
  anti_air   = 1
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 2 current_size = 2 }
  ic         = 2
}              # コロンボ
province = { id = 1519 ic = 1 } # カラグプル
province =
{ id          = 1591
  anti_air    = 2
  air_base    = { size = 4 current_size = 4 }
  naval_base  = { size = 4 current_size = 4 }
  coastalfort = 3
  oilpool     = 1000
  supplypool  = 2000
  ic          = 3
}               # トリンコマリー
province =
{ id            = 1694
  naval_base    = { size = 4 current_size = 4 }
  air_base      = { size = 2 current_size = 2 }
}                 # マドラス
province = { id = 1758 ic = 2 } # コーチ
province =
{ id         = 1817
  naval_base = { size = 4 current_size = 4 }
  air_base   = { size = 2 current_size = 2 }
}              # マレ
province =
{ id       = 2592
  air_base = { size = 2 current_size = 2 }
}            # ディブルーガル

#####################
# Country main data #
#####################

country =
{ tag                 = IND
  manpower            = 360
  energy              = 8000
  metal               = 3500
  rare_materials      = 5000
  oil                 = 4000
  supplies            = 6000
  money               = 350
  capital             = 1469
  regular_id          = U04
  transports          = 250
  escorts             = 150
  nationalprovinces   = { 1458 1459 1460 1461 1462 1463 1464 1465 1466 1467 1468 1469 1470 1471 1472 1478 1479 1505 1506 1507 1508 1509 1510 1511 1513
                          1514 1515 599  685  687  736  1476 1477 1705 2092 2199 593  597  669  697  726  769  785  1699 611  631  635  714  722  1701
                          1721 2091 2094 2133 773  774  775  1694 1758 2106 702  703  771  1695 2099 2175 729  791  2124 2198 781  1284 1287 1454 1456
                          1457 1519 2104 2592 1518 2085 1512 777  1817
                        }
  ownedprovinces      = { 1458 1459 1460 1461 1462 1463 1464 1465 1466 1467 1468 1469 1470 1471 1472 1478 1479 1505 1506 1507 1508 1509 1510 1511 1513
                          1514 1515 599  685  687  736  1476 1477 1705 2092 2199 593  597  669  697  726  769  785  1699 611  631  635  714  722  1701
                          1721 2091 2094 2133 773  774  775  1694 1758 2106 702  703  771  1695 2099 2175 729  791  2124 2198 781  1284 1287 1454 1456
                          1457 1519 2104 2592 1518 2085 1512 777  1817
                        }
  controlledprovinces = { 1458 1459 1460 1461 1462 1463 1464 1465 1466 1467 1468 1469 1470 1471 1472 1478 1479 1505 1506 1507 1508 1509 1510 1511 1513
                          1514 1515 599  685  687  736  1476 1477 1705 2092 2199 593  597  669  697  726  769  785  1699 611  631  635  714  722  1701
                          1721 2091 2094 2133 773  774  775  1694 1758 2106 702  703  771  1695 2099 2175 729  791  2124 2198 781  1284 1287 1454 1456
                          1457 1519 2104 2592 1518 2085 1512 777  1817
                        }
  techapps            = { 10050 1010  10190 1020  1110  1120  1210  1550  1650  3010  3020  3110  3460  3810  3910  4390  50020 5010  5020  50290
                          50300 5090  5410  5600  7010  80080 8010  8020  8030  8040  8050  8060  8070  8080  9010  9020  9030  9040  9050  9060
                          9340  6050  6060  6070  6080  6090  1220  1660  1760
                        }
  policy              = { democratic = 3 political_left = 3 freedom = 2 free_market = 3 professional_army = 9 defense_lobby = 8 interventionism = 9 }
    landunit = {
	name = "2th Mountain Infantry Division"
	id = { type = 24581 id = 1 }
	location = 1457
	division = {
	id = { type = 24581 id = 2 }
	name = "1th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 3 }
	name = "2th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 4 }
	name = "3th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 5 }
	name = "4th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 6 }
	name = "5th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 7 }
	name = "6th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "56th Mountain Infantry Division"
	id = { type = 24581 id = 8 }
	location = 1456
	division = {
	id = { type = 24581 id = 9 }
	name = "7th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 10 }
	name = "8th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 11 }
	name = "9th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 12 }
	name = "10th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 13 }
	name = "11th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 14 }
	name = "12th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "57th Mountain Infantry Division"
	id = { type = 24581 id = 15 }
	location = 1454
	division = {
	id = { type = 24581 id = 16 }
	name = "13th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 17 }
	name = "14th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 18 }
	name = "15th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 19 }
	name = "16th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 20 }
	name = "17th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 21 }
	name = "18th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "5th Mountain Infantry Division"
	id = { type = 24581 id = 22 }
	location = 1284
	division = {
	id = { type = 24581 id = 23 }
	name = "19th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 24 }
	name = "20th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 25 }
	name = "21th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 26 }
	name = "22th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 27 }
	name = "23th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 28 }
	name = "24th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "21th Mountain Infantry Division"
	id = { type = 24581 id = 29 }
	location = 2592
	division = {
	id = { type = 24581 id = 30 }
	name = "25th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 31 }
	name = "26th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 32 }
	name = "27th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 33 }
	name = "28th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 34 }
	name = "29th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 35 }
	name = "30th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "71th Mountain Infantry Division"
	id = { type = 24581 id = 36 }
	location = 2104
	division = {
	id = { type = 24581 id = 37 }
	name = "31th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 38 }
	name = "32th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 39 }
	name = "33th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 40 }
	name = "34th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 41 }
	name = "35th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 42 }
	name = "36th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "17th Mountain Infantry Division"
	id = { type = 24581 id = 43 }
	location = 1287
	division = {
	id = { type = 24581 id = 44 }
	name = "37th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 45 }
	name = "38th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 46 }
	name = "39th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 47 }
	name = "40th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 48 }
	name = "41th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 49 }
	name = "42th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "20th Mountain Infantry Division"
	id = { type = 24581 id = 50 }
	location = 1472
	division = {
	id = { type = 24581 id = 51 }
	name = "43th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 52 }
	name = "44th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 53 }
	name = "45th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 54 }
	name = "46th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 55 }
	name = "47th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 56 }
	name = "48th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "27th Mountain Infantry Division"
	id = { type = 24581 id = 57 }
	location = 2094
	division = {
	id = { type = 24581 id = 58 }
	name = "49th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 59 }
	name = "50th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 60 }
	name = "51th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 61 }
	name = "52th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 62 }
	name = "53th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 63 }
	name = "54th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
  }
landunit = {
	name = "8th Mountain Infantry Division"
	id = { type = 24581 id = 64 }
	location = 736
	division = {
	id = { type = 24581 id = 65 }
	name = "55th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 66 }
	name = "56th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 67 }
	name = "57th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 68 }
	name = "58th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 69 }
	name = "59th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 70 }
	name = "60th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "28th Mountain Infantry Division"
	id = { type = 24581 id = 71 }
	location = 687
	division = {
	id = { type = 24581 id = 72 }
	name = "61th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 73 }
	name = "62th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 74 }
	name = "63th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 75 }
	name = "64th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 76 }
	name = "65th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 77 }
	name = "66th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
}
landunit = {
	name = "6th Mountain Infantry Division"
	id = { type = 24581 id = 78 }
	location = 703
	division = {
	id = { type = 24581 id = 79 }
	name = "67th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 80 }
	name = "68th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 81 }
	name = "69th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 82 }
	name = "70th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 83 }
	name = "71th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
	division = {
	id = { type = 24581 id = 84 }
	name = "72th Mountain Infantry Batallion"
	type = mechanized
	model = 22
	experience = 50
	}
  }
landunit = {
	name = "23th Infantry Division"
	id = { type = 24581 id = 85 }
	location = 1457
	division = {
	id = { type = 24581 id = 86 }
	name = "1th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 87 }
	name = "2th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 88 }
	name = "3th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 89 }
	name = "4th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 90 }
	name = "5th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 91 }
	name = "6th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "3th Infantry Division"
	id = { type = 24581 id = 92 }
	location = 1705
	division = {
	id = { type = 24581 id = 93 }
	name = "7th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 94 }
	name = "8th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 95 }
	name = "9th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 96 }
	name = "10th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 97 }
	name = "11th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 98 }
	name = "12th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "19th Infantry Division"
	id = { type = 24581 id = 99 }
	location = 1476
	division = {
	id = { type = 24581 id = 100 }
	name = "13th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 101 }
	name = "14th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 102 }
	name = "15th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 103 }
	name = "16th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 104 }
	name = "17th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 105 }
	name = "18th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "10th Infantry Division"
	id = { type = 24581 id = 106 }
	location = 1477
	division = {
	id = { type = 24581 id = 107 }
	name = "19th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 108 }
	name = "20th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 109 }
	name = "21th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 110 }
	name = "22th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 111 }
	name = "23th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 112 }
	name = "24th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "25th Infantry Division"
	id = { type = 24581 id = 113 }
	location = 1478
	division = {
	id = { type = 24581 id = 114 }
	name = "25th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 115 }
	name = "26th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 116 }
	name = "27th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 117 }
	name = "28th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 118 }
	name = "29th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 119 }
	name = "30th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "39th Infantry Division"
	id = { type = 24581 id = 120 }
	location = 1469
	division = {
	id = { type = 24581 id = 121 }
	name = "31th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 122 }
	name = "32th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 123 }
	name = "33th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 124 }
	name = "34th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 125 }
	name = "35th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 126 }
	name = "36th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "11th Infantry Division"
	id = { type = 24581 id = 127 }
	location = 1694
	division = {
	id = { type = 24581 id = 128 }
	name = "37th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 129 }
	name = "38th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 130 }
	name = "39th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 131 }
	name = "40th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 132 }
	name = "41th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 133 }
	name = "42th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "54th Infantry Division"
	id = { type = 24581 id = 134 }
	location = 1513
	division = {
	id = { type = 24581 id = 135 }
	name = "43th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 136 }
	name = "44th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 137 }
	name = "45th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 138 }
	name = "46th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 139 }
	name = "47th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 140 }
	name = "48th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "4th Infantry Division"
	id = { type = 24581 id = 141 }
	location = 1505
	division = {
	id = { type = 24581 id = 142 }
	name = "49th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 143 }
	name = "50th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 144 }
	name = "51th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 145 }
	name = "52th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 146 }
	name = "53th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 147 }
	name = "54th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "16th Infantry Division"
	id = { type = 24581 id = 148 }
	location = 1512
	division = {
	id = { type = 24581 id = 149 }
	name = "55th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 150 }
	name = "56th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 151 }
	name = "57th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 152 }
	name = "58th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 153 }
	name = "59th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 154 }
	name = "60th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "22th Infantry Division"
	id = { type = 24581 id = 155 }
	location = 1479
	division = {
	id = { type = 24581 id = 156 }
	name = "61th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 157 }
	name = "62th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 158 }
	name = "63th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 159 }
	name = "64th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 160 }
	name = "65th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 161 }
	name = "66th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "26th Infantry Division"
	id = { type = 24581 id = 162 }
	location = 593
	division = {
	id = { type = 24581 id = 163 }
	name = "67th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 164 }
	name = "68th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 165 }
	name = "69th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 166 }
	name = "70th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 167 }
	name = "71th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 168 }
	name = "72th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "29th Infantry Division"
	id = { type = 24581 id = 169 }
	location = 593
	division = {
	id = { type = 24581 id = 170 }
	name = "73th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 171 }
	name = "74th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 172 }
	name = "75th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 173 }
	name = "76th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 174 }
	name = "77th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 175 }
	name = "78th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "7th Infantry Division"
	id = { type = 24581 id = 176 }
	location = 1479
	division = {
	id = { type = 24581 id = 177 }
	name = "79th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 178 }
	name = "80th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 179 }
	name = "81th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 180 }
	name = "82th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 181 }
	name = "83th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 182 }
	name = "84th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "9th Infantry Division"
	id = { type = 24581 id = 183 }
	location = 1721
	division = {
	id = { type = 24581 id = 184 }
	name = "85th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 185 }
	name = "86th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 186 }
	name = "87th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 187 }
	name = "88th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 188 }
	name = "89th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 189 }
	name = "90th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
}
landunit = {
	name = "15th Infantry Division"
	id = { type = 24581 id = 190 }
	location = 1479
	division = {
	id = { type = 24581 id = 191 }
	name = "91th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 192 }
	name = "92th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 193 }
	name = "93th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 194 }
	name = "94th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 195 }
	name = "95th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24581 id = 196 }
	name = "96th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
  }
landunit = {
	name = "12th Light Mechanized Infantry Division"
	id = { type = 24581 id = 197 }
	location = 702
	division = {
	id = { type = 24581 id = 198 }
	name = "1th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 199 }
	name = "2th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 200 }
	name = "3th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 201 }
	name = "4th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 202 }
	name = "5th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 203 }
	name = "6th Mechanized Light Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
}
landunit = {
	name = "36th Light Mechanized Infantry Division"
	id = { type = 24581 id = 204 }
	location = 1515
	division = {
	id = { type = 24581 id = 205 }
	name = "7th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 206 }
	name = "8th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 207 }
	name = "9th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 208 }
	name = "10th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 209 }
	name = "11th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 210 }
	name = "12th Mechanized Light Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
}
landunit = {
	name = "18th Light Mechanized Infantry Division"
	id = { type = 24581 id = 211 }
	location = 1464
	division = {
	id = { type = 24581 id = 212 }
	name = "13th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 213 }
	name = "14th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 214 }
	name = "15th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 215 }
	name = "16th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 216 }
	name = "17th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 217 }
	name = "18th Mechanized Light Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
}
landunit = {
	name = "24th Light Mechanized Infantry Division"
	id = { type = 24581 id = 218 }
	location = 1464
	division = {
	id = { type = 24581 id = 219 }
	name = "19th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 220 }
	name = "20th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 221 }
	name = "21th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 222 }
	name = "22th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 223 }
	name = "23th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 224 }
	name = "24th Mechanized Light Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
}
landunit = {
	name = "14th Light Mechanized Infantry Division"
	id = { type = 24581 id = 225 }
	location = 1479
	division = {
	id = { type = 24581 id = 226 }
	name = "25th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 227 }
	name = "26th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 228 }
	name = "27th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 229 }
	name = "28th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 230 }
	name = "29th Mechanized Light Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 231 }
	name = "30th Mechanized Light Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
  }
landunit = {
	name = "31th Armored Division"
	id = { type = 24581 id = 232 }
	location = 1510
	division = {
	id = { type = 24581 id = 233 }
	name = "1th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 234 }
	name = "2th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 235 }
	name = "3th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 236 }
	name = "1th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 237 }
	name = "2th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 238 }
	name = "3th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
}
landunit = {
	name = "1th Armored Division"
	id = { type = 24581 id = 239 }
	location = 1479
	division = {
	id = { type = 24581 id = 240 }
	name = "4th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 241 }
	name = "5th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 242 }
	name = "6th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 243 }
	name = "4th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 244 }
	name = "5th Mechanized Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24581 id = 245 }
	name = "6th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
   }
  }
}
