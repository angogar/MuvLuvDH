##############################
# Country definition for DFR #
##############################
province =
{ id       = 296
  anti_air = 2
  air_base = { size = 4 current_size = 4 }
}            # ロストック
province =
{ id       = 298
  anti_air = 2
  air_base = { size = 4 current_size = 4 }
}            # マグデブルク
province =
{ id       = 300
  anti_air = 2
  air_base = { size = 10 current_size = 10 }
}            # ベルリン
province =
{ id            = 302
  anti_air      = 1
  air_base      = { size = 4 current_size = 4 }
  naval_base    = { size = 4 current_size = 4 }
}                 # シュテッティン
province =
{ id         = 303
  anti_air   = 2
  naval_base = { size = 4 current_size = 4 }
}              # ダンツィヒ
province =
{ id         = 304
  anti_air   = 2
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 4 current_size = 4 }
}              # エルビンク
province = { id = 305 anti_air = 1 } # プウォツク
province =
{ id         = 306
  anti_air   = 2
  naval_base = { size = 4 current_size = 4 }
  air_base   = { size = 4 current_size = 4 }
}              # グディニャ
province = { id = 307 anti_air = 1 } # トルン
province = { id = 308 anti_air = 2 landfort = 2 } # メーゼリッツ
province =
{ id              = 310
  anti_air        = 1
  air_base        = { size = 4 current_size = 4 }
}                   # ライプツィヒ
province =
{ id       = 312
  anti_air = 1
  air_base = { size = 4 current_size = 4 }
}            # ニュルンベルク
province =
{ id       = 313
  anti_air = 3
  air_base = { size = 4 current_size = 4 }
}            # フランクフルト・アム・マイン
province =
{ id       = 314
  anti_air = 2
  air_base = { size = 4 current_size = 4 }
}            # シュッツトガルト
province = { id = 76 anti_air = 2 } # デュッセルドルフ
province =
{ id         = 80
  anti_air   = 2
  air_base   = { size = 4 current_size = 4 }
  naval_base = { size = 10 current_size = 10 }
}              # ヴィルヘルムスハーフェン
province =
{ id       = 81
  anti_air = 1
  air_base = { size = 4 current_size = 4 }
}            # ミュンスター
province =
{ id       = 82
  anti_air = 1
  air_base = { size = 4 current_size = 4 }
}            # カッセル
province =
{ id         = 87
  anti_air   = 2
  air_base   = { size = 2 current_size = 2 }
  naval_base = { size = 8 current_size = 8 }
}              # ブレーメン
province =
{ id         = 88
  anti_air   = 2
  naval_base = { size = 8 current_size = 8 }
}              # ハンブルク
province = { id = 89 anti_air = 1 } # リューベック
province =
{ id         = 90
  anti_air   = 2
  air_base   = { size = 4 current_size = 4 }
  naval_base = { size = 10 current_size = 10 }
}              # キール
#####################
# Country main data #
#####################

country =
{ tag                 = DFR
  manpower            = 4250
  energy              = 58000
  metal               = 36000
  rare_materials      = 45000
  oil                 = 12000
  supplies            = 8000
  money               = 3500
  capital             = 75
  transports          = 1800
  escorts             = 1500
  regular_id          = U02
  nationalprovinces   = { 66 67 74 75 76 80 81 82 83 85 86 87 88 89 90 312 313 314 315 374 375 376 547 970 989 1014 1015 1016 1018 1405 }
  ownedprovinces      = { 66 67 74 75 76 80 81 82 83 85 86 87 88 89 90 312 313 314 315 374 375 376 547 970 989 1014 1015 1016 1018 1405 }
  controlledprovinces = { 66 67 74 75 76 80 81 82 83 85 86 87 88 89 90 312 313 314 315 374 375 376 547 970 989 1014 1015 1016 1018 1405 }
  techapps            = { 10050 10060 1010  10190 1020  10200 1030    1110  1120 
                          1210  1220  1470  1490  1510  1530  1550  1560  1650  1660
                          1750  1760  1850  1860  1950  1960
                          3010  3020  30410 30420 30510 30520 3110 
                          3120  3210  3220  3460  3470
                          3560      3910  3920
                          3940  40100 40110 4390  4400  4010  4020  4110  4120  4210  4220  4640  4650  4820  4830
                          4560  50020 50030   50070 50080  5010    50120 5020  50290 50300 50310
                          5090  5100  5170  5180  5250  5260  5330  5340  5410  5420
                          5490  5500  5600  5610  5680  5690  5760  5770  5780  5790  5800  5810
                          5910  5920  7010  80080 80090 8010  80100 80160 8020  8030  8040  8050  8060  8070  8280  8290  8300  8310  8320  8330  9010  90140
                          90190 9020  9030  9040  9050  9060  9070  9190  9200  9210  9340  9350  9360  9370  9380  9390  9400  9520  9530  9540 
                          9550  9670  9680  9690  9700  9710  9720  9840  9850  9860  9990  2000  2210  3000  4000  5000
                        }
  inventions          = { }
  policy =
  { democratic        = 10
    political_left    = 2
    freedom           = 9
    free_market       = 9
    professional_army = 9
    defense_lobby     = 10
    interventionism   = 9
  }
landunit = {
	name = "1th Armored Division"
	id = { type = 10010 id = 1 }
	location = 86
	division = {
	id = { type = 10010 id = 2 }
	name = "1th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 3 }
	name = "2th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 4 }
	name = "3th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 5 }
	name = "1th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 6 }
	name = "2th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 7 }
	name = "3th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
        #locked = yes
	}
  }
landunit = {
	name = "3th Armored Division"
	id = { type = 10010 id = 8 }
	location = 87
	division = {
	id = { type = 10010 id = 9 }
	name = "4th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 10 }
	name = "5th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 11 }
	name = "6th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 12 }
	name = "4th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 13 }
	name = "5th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 14 }
	name = "6th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
        #locked = yes
	}
  }
landunit = {
	name = "7th Armored Division"
	id = { type = 10010 id = 15 }
	location = 67
	division = {
	id = { type = 10010 id = 16 }
	name = "7th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 17 }
	name = "8th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 18 }
	name = "9th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 19 }
	name = "7th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 20 }
	name = "8th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 21 }
	name = "9th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
        #locked = yes
	}
  }
    landunit = {
	name = "5th Armored Division"
	id = { type = 10010 id = 22 }
	location = 1018
	division = {
	id = { type = 10010 id = 23 }
	name = "10th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 24 }
	name = "11th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 25 }
	name = "12th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 26 }
	name = "10th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 27 }
	name = "11th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 28 }
	name = "12th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
        #locked = yes
	}
  }
    landunit = {
	name = "12th Armored Division"
	id = { type = 10010 id = 29 }
	location = 547
	division = {
	id = { type = 10010 id = 30 }
	name = "13th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 31 }
	name = "14th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 32 }
	name = "15th Armored Batallion"
	type = armor
	model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 33 }
	name = "13th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 34 }
	name = "14th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	extra = anti_air
	brigade_model = 1
	experience = 0
        #locked = yes
	}
	division = {
	id = { type = 10010 id = 35 }
	name = "15th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
        #locked = yes
	}
  }
landunit = {
	name = "6th Panzer Grenadier Division"
	id = { type = 10010 id = 43 }
	location = 90
	division = {
	id = { type = 10010 id = 44 }
	name = "16th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 45 }
	name = "17th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 46 }
	name = "16th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 47 }
	name = "17th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 48 }
	name = "18th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 49 }
	name = "19th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
}
landunit = {
	name = "11th Panzer Grenadier Division"
	id = { type = 10010 id = 50 }
	location = 80
	division = {
	id = { type = 10010 id = 51 }
	name = "18th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 52 }
	name = "19th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 53 }
	name = "20th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 54 }
	name = "21th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 55 }
	name = "22th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 56 }
	name = "23th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
}
landunit = {
	name = "4th Panzer Grenadier Division"
	id = { type = 10010 id = 57 }
	location = 375
	division = {
	id = { type = 10010 id = 58 }
	name = "20th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 59 }
	name = "21th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 60 }
	name = "24th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 61 }
	name = "25th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 62 }
	name = "26th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 63 }
	name = "27th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
}
landunit = {
	name = "10th Panzer Grenadier Division"
	id = { type = 10010 id = 64 }
	location = 314
	division = {
	id = { type = 10010 id = 65 }
	name = "22th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 66 }
	name = "23th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 67 }
	name = "28th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 68 }
	name = "29th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 69 }
	name = "30th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 70 }
	name = "31th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
}
landunit = {
	name = "12th Panzer Grenadier Division"
	id = { type = 10010 id = 71 }
	location = 547
	division = {
	id = { type = 10010 id = 72 }
	name = "24th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 73 }
	name = "25th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 74 }
	name = "32th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 75 }
	name = "33th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 76 }
	name = "34th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 77 }
	name = "35th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
}
landunit = {
	name = "2th Jaeger Division"
	id = { type = 10010 id = 78 }
	location = 313
	division = {
	id = { type = 10010 id = 79 }
	name = "26th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 80 }
	name = "27th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 81 }
	name = "36th Armored Grenadier Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 82 }
	name = "37th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 83 }
	name = "38th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 84 }
	name = "39th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
}
landunit = {
	name = "1th Mountain Division"
	id = { type = 10010 id = 85 }
	location = 376
	division = {
	id = { type = 10010 id = 86 }
	name = "28th Armored Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 87 }
	name = "1th Mountain Infantry Batallion"
	type = mechanized
	model = 21
	experience = 0
	}
	division = {
	id = { type = 10010 id = 88 }
	name = "2th Mountain Infantry Batallion"
	type = mechanized
	model = 21
	experience = 0
	}
	division = {
	id = { type = 10010 id = 89 }
	name = "3th Mountain Infantry Batallion"
	type = mechanized
	model = 21
	experience = 0
	}
	division = {
	id = { type = 10010 id = 90 }
	name = "40th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 10010 id = 91 }
	name = "41th Armored Grenadier Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
}
  ##51旅団 キール
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5001 }
    name = "512th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5002 }
    name = "513th Tank Batallion"
    strength = 100
    type = armor
    model = 2
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5003 }
    name = "514th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5004 }
    name = "515th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  ##61旅団 キール
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5005 }
    name = "613th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5006 }
    name = "614th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5007 }
    name = "615th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  ##52旅団 ハノーファー
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5008 }
    name = "522th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5009 }
    name = "523th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5010 }
    name = "524th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5011 }
    name = "525th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  ##62旅団 ハノーファー
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5012 }
    name = "623th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5013 }
    name = "624th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5014 }
    name = "625th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  ##53旅団 デュッセルドルフ
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5015 }
    name = "532th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5016 }
    name = "533th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5017 }
    name = "534th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5018 }
    name = "535th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  ##63旅団 デュッセルドルフ
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5019 }
    name = "633th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5020 }
    name = "634th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5021 }
    name = "635th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  ##54旅団 マインツ
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5022 }
    name = "542th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5023 }
    name = "543th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5024 }
    name = "544th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5025 }
    name = "545th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  ##64旅団 マインツ
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5026 }
    name = "643th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5027 }
    name = "644th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5028 }
    name = "645th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  ##55旅団 シュトゥットガルト
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5029 }
    name = "552th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5030 }
    name = "553th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5031 }
    name = "554th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5032 }
    name = "555th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  ##65旅団 マインツ
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5033 }
    name = "652th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5034 }
    name = "653th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5035 }
    name = "654th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  ##56旅団 ミュンヘン
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5036 }
    name = "561th Armored Grenadier Batallion"
    strength = 100
    type = mechanized
    model = 2
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5037 }
    name = "562th Armored Grenadier Batallion"
    strength = 100
    type = mechanized
    model = 2
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5038 }
    name = "563th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5039 }
    name = "564th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  ##66旅団 ミュンヘン
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5040 }
    name = "663th Tank Batallion"
    strength = 100
    type = armor
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5041 }
    name = "664th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
  landdivision = {
    dormant = yes
    id = { type = 10010 id = 5042 }
    name = "665th Armoured Jaeger Division"
    strength = 100
    type = mechanized
    model = 1
  }
}
