
##############################
# Country definition for PER #
##############################

#####################
# Country main data #
#####################

country =
{ tag                 = PER
  manpower            = 420
  energy              = 12000
  metal               = 18000
  rare_materials      = 15000
  oil                 = 50000
  supplies            = 80000
  money               = 1800
  capital             = 1502
  regular_id          = U03
  transports          = 400
  escorts             = 300
  nationalprovinces   = { 1495 1496 1497 1498 1499 1500 1501 1502 1503 1787 1788 1789 1825 1826 1859 }
  ownedprovinces      = { 1495 1496 1497 1498 1499 1500 1501 1502 1503 1787 1788 1789 1825 1826 1859 }
  controlledprovinces = { 1495 1496 1497 1498 1499 1500 1501 1502 1503 1787 1788 1789 1825 1826 1859 }
  techapps            = { 10050 1010  10190 1020  1110  1120  1210  1550  1650  3010  3020  3110  3460  3810  3910  4390  50020 5010  5020  50290
                          50300 5090  5410  5600  7010  80080 8010  8020  8030  8040  8050  8060  8070  8080  9010 
                          9340  6050  6060  6070  6080  6090  1220  1660  1760
                        }
  inventions          = { }
  policy              = { democratic = 3 political_left = 2 freedom = 3 free_market = 2 professional_army = 9 defense_lobby = 8 interventionism = 8 }

landunit = {
	name = "16th Armored Division"
	id = { type = 24591 id = 1 }
	location = 1500
	division = {
	id = { type = 24591 id = 2 }
	name = "1th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 3 }
	name = "2th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 4 }
	name = "3th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 5 }
	name = "1th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 6 }
	name = "2th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 7 }
	name = "3th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "81th Armored Division"
	id = { type = 24591 id = 8 }
	location = 1503
	division = {
	id = { type = 24591 id = 9 }
	name = "4th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 10 }
	name = "5th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 11 }
	name = "6th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 12 }
	name = "4th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 13 }
	name = "5th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 14 }
	name = "6th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "88th Armored Division"
	id = { type = 24591 id = 15 }
	location = 1859
	division = {
	id = { type = 24591 id = 16 }
	name = "7th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 17 }
	name = "8th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 18 }
	name = "9th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 19 }
	name = "7th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 20 }
	name = "8th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 21 }
	name = "9th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "92th Armored Division"
	id = { type = 24591 id = 22 }
	location = 1788
	division = {
	id = { type = 24591 id = 23 }
	name = "10th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 24 }
	name = "11th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 25 }
	name = "12th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 26 }
	name = "10th Mechanized Infantry Batallion"
	type = light_armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 27 }
	name = "11th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 28 }
	name = "12th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "28th Infantry Division"
	id = { type = 24591 id = 29 }
	location = 1498
	division = {
	id = { type = 24591 id = 30 }
	name = "13th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 31 }
	name = "1th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 32 }
	name = "2th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 33 }
	name = "3th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 34 }
	name = "13th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 35 }
	name = "14th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "30th Infantry Division"
	id = { type = 24591 id = 36 }
	location = 1495
	division = {
	id = { type = 24591 id = 37 }
	name = "14th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 38 }
	name = "4th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 39 }
	name = "5th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 40 }
	name = "6th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 41 }
	name = "15th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 42 }
	name = "16th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "64th Infantry Division"
	id = { type = 24591 id = 43 }
	location = 1825
	division = {
	id = { type = 24591 id = 44 }
	name = "15th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 45 }
	name = "7th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 46 }
	name = "8th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 47 }
	name = "9th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 48 }
	name = "17th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 49 }
	name = "18th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "77th Infantry Division"
	id = { type = 24591 id = 50 }
	location = 1789
	division = {
	id = { type = 24591 id = 51 }
	name = "16th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 52 }
	name = "10th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 53 }
	name = "11th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 54 }
	name = "12th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 55 }
	name = "19th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 56 }
	name = "20th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "84th Infantry Division"
	id = { type = 24591 id = 57 }
	location = 1500
	division = {
	id = { type = 24591 id = 58 }
	name = "17th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 59 }
	name = "13th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 60 }
	name = "14th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 61 }
	name = "15th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 62 }
	name = "21th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 63 }
	name = "22th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "1th Guards Division"
	id = { type = 24591 id = 64 }
	location = 1502
	division = {
	id = { type = 24591 id = 65 }
	name = "18th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 66 }
	name = "16th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 67 }
	name = "17th Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 68 }
	name = "18th Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 69 }
	name = "23th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 70 }
	name = "24th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
}
landunit = {
	name = "2th Guards Division"
	id = { type = 24591 id = 71 }
	location = 1501
	division = {
	id = { type = 24591 id = 72 }
	name = "19th Tank Batallion"
	type = armor
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 73 }
	name = "19th Infantry Batallion"
	type = mechanized
	model = 22
	experience = 0
	}
	division = {
	id = { type = 24591 id = 74 }
	name = "20th Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 75 }
	name = "21th Infantry Batallion"
	type = mechanized
	model = 1
	experience = 0
	}
	division = {
	id = { type = 24591 id = 76 }
	name = "25th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
	division = {
	id = { type = 24591 id = 77 }
	name = "26th Mechanized Infantry Batallion"
	type = mechanized
	model = 0
	experience = 0
	}
  }
}
