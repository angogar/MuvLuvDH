##############################################################
##
## Imperial Japan Flavor Events(24001~25000)
##
##############################################################
## 
## id = 24001	Relationship with the Unified Chinese Front�{
## id = 24002	Relationship with the EU�{
## id = 24003	Relationship with the US�{
## id = 24004	Relationthip with the UN�{
## id = 24005	Relationthip with the Soviet Union�{
## id = 24006	Relationthip with the Greater East Asia Union�{
## id = 24007	Relationthip with Australia�{
## id = 24008	Relationthip with the AU�{
## id = 24009	Relationthip with the MEU�{
## id = 24010	Relationship with the Unified Chinese Front�]
## id = 24011	Relationship with the EU�]
## id = 24012	Relationship with the US�]
## id = 24013	Relationthip with the UN�]
## id = 24014	Relationthip with the Soviet Union�]
## id = 24015	Relationthip with the Greater East Asia Union�]
## id = 24016	Relationthip with Australia�]
## id = 24017	Relationthip with the AU�]
## id = 24018	Relationthip with the MEU�]
## id = 24019
## id = 24020
## id = 24021
## id = 24022
## id = 24023
## id = 24024
## id = 24025
## id = 24026
## id = 24027
## id = 24028
## id = 24029
## id = 24030
## id = 24031
## id = 24032
## id = 24033
## id = 24034
## id = 24035
## id = 24036
## id = 24037
## id = 24038
## id = 24039
## id = 24040
## id = 24041
## id = 24042
## id = 24043
## id = 24044
## id = 24045
## id = 24046
## id = 24047
## id = 24048
## id = 24049
## id = 24050
##
##############################################################

## Reconciliation with other countries
event = {
	id = 24001
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationship with the Unified Chinese Front"
	desc = "EVT_24001_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U05 value = 10 } 
 	}
}

event = {
	id = 24002
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationship with the EU"
	desc = "EVT_24002_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U02 value = 10 } 
 	}
}

event = {
	id = 24003
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationship with the US"
	desc = "EVT_24003_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = USA value = 10 } 
 	}
}

event = {
	id = 24004
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with the UN"
	desc = "EVT_24004_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U06 value = 10 } 
	}
}

event = {
	id = 24005
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with the Soviet Union"
	desc = "EVT_24005_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = SOV value = 10 } 
        }
}

event = {
	id = 24006
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with the Greater East Asia Union"
	desc = "EVT_24006_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U04 value = 10 } 
        }
}

event = {
	id = 24007
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with Australia"
	desc = "EVT_24007_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = AST value = 10 } 
        }
}

event = {
	id = 24008
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with the AU"
	desc = "EVT_24008_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U01 value = 10 } 
        }
}

event = {
	id = 24009
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with the MEU"
	desc = "EVT_24009_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = JAP value = 10 } 
        }
}

event = {
	id = 24010
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationship with the Unified Chinese Front"
	desc = "EVT_24010_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U05 value = -10 } 
 	}
}

event = {
	id = 24011
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationship with the EU"
	desc = "EVT_24011_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U02 value = -10 } 
 	}
}

event = {
	id = 24012
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationship with the US"
	desc = "EVT_24012_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = USA value = -10 } 
 	}
}

event = {
	id = 24013
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with the UN"
	desc = "EVT_24013_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U06 value = -10 } 
	}
}

event = {
	id = 24014
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with the Soviet Union"
	desc = "EVT_24014_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = SOV value = -10 } 
        }
}

event = {
	id = 24015
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with the Greater East Asia Union"
	desc = "EVT_24015_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U04 value = -10 } 
        }
}

event = {
	id = 24016
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with Australia"
	desc = "EVT_24016_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = AST value = -10 } 
        }
}

event = {
	id = 24017
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationthip with the AU"
	desc = "EVT_24017_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = U01 value = -10 } 
        }
}

event = {
	id = 24018
	random = yes
	country = U03
	trigger = {
	}

	name = "Relationship with Japan"
	desc = "EVT_24018_DESC"
	style = 0

	action_a = {
		name = "Nice!"
		command = { type = relation which = JAP value = -10 } 
        }
}