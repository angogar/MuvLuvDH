# 0 - Laser class
model = {
	cost 				= 2
	buildtime	 		= 35
	manpower 			= 0
	defensiveness 		= 8
	airdefence				= 9999
	softattack			= 1
	hardattack			= 1
	airattack			= 9999
	supplyconsumption 	= 0
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 1 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 2 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 3 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 4 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 5 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 6 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 7 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 8 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 9 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 10 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 11 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 12 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 13 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 14 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 15 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 16 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 17 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 18 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 19 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 20 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 21 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 22 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 23 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 24 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 25 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 26 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 27 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 28 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 29 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
# 30 - -----
model = {
	cost 				= 10
	buildtime	 		= 120
	manpower 			= 2
	maxspeed 			= -4
	defensiveness 		= 8
	toughness 			= 2
	softness			= -40
	softattack			= 7
	hardattack			= 8
	supplyconsumption 	= 0.6
	fuelconsumption		= 2
	upgrade_time_factor = 0.5
	upgrade_cost_factor = 1
}
